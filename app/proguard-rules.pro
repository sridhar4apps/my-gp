# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in C:\Users\2136\AppData\Local\Android\Sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFilex
-keepclassmembers,allowobfuscation class * {
    @com.google.gson.annotations.SerializedName <fields>;
  }
-keep,allowobfuscation interface com.google.gson.annotations.SerializedName
-keepclassmembers enum * { *; }



-optimizationpasses 5
  -dontusemixedcaseclassnames
  -dontskipnonpubliclibraryclasses
   -dontpreverify
 -verbose
   -optimizations !code/simplification/arithmetic,!field/*,!class/merging/*

   -keep public class * extends android.app.Activity
  -keep public class * extends android.app.Application
   -keep public class * extends android.app.Service
  -keep public class * extends android.content.BroadcastReceiver
   -keep public class * extends android.content.ContentProvider
   -keep public class * extends android.app.backup.BackupAgentHelper
  -keep public class * extends android.preference.Preference

   #keep all classes that might be used in XML layouts
 -keep public class * extends android.view.View
  -keep public class * extends android.app.Fragment



-keepnames class * implements android.os.Parcelable {
    public static final ** CREATOR;
}

-keepclassmembernames class * {
     public protected <methods>;
 }

  -keepclasseswithmembernames class * {
     native <methods>;
  }

  -keepclasseswithmembernames class * {
       public <init>(android.content.Context, android.util.AttributeSet);
   }

  -keepclasseswithmembernames class * {
      public <init>(android.content.Context, android.util.AttributeSet, int);
 }


  -keepclassmembers enum * {
     public static **[] values();
       public static ** valueOf(java.lang.String);
  }

 -keep class * implements android.os.Parcelable {
    public static final android.os.Parcelable$Creator *;
  }

  -dontwarn **CompatHoneycomb
  -dontwarn org.htmlcleaner.*
  -dontwarn android.support.**
  -dontwarn com.google.**
  -dontwarn org.apache.http.*
  -dontwarn android.net.http.**
  -dontwarn org.apache.commons.**
  -dontwarn com.crashlytics.**
  -dontwarn okio.**
  -dontwarn okhttp3.**
  -dontwarn org.codehaus.**
  -dontwarn jp.co.cyberagent.android.gpuimage.**

-keep class com.village.mygp.viewmodel.** { *; }
-keep class com.village.mygp.view.** { *; }
-keep class com.village.mygp.model.** { *; }
-keep class com.village.mygp.application.** { *; }
-dontoptimize

-keep class com.firebase.** { *; }
-keep class org.apache.** { *; }

  -dontwarn com.google.android.material.**
  -dontnote com.google.android.material.**

  -dontwarn androidx.**
  -keep public class * implements androidx.versionedparcelable.VersionedParcelable
  -keep class butterknife.*
  -keepclasseswithmembernames class * { @butterknife.* <methods>; }
  -keepclasseswithmembernames class * { @butterknife.* <fields>; }




#Butterknife
-keep class butterknife.** { *; }
-dontwarn butterknife.internal.**
-keep class **$$ViewBinder { *; }

-keepclasseswithmembernames class * {
    @butterknife.* <fields>;
}

-keepclasseswithmembernames class * {
    @butterknife.* <methods>;
}



-dontnote java.lang.invoke.**
-dontoptimize
-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-verbose
# Preserve some attributes that may be required for reflection.
-keepattributes *Annotation*,Signature,InnerClasses,EnclosingMethod

-dontnote com.android.vending.licensing.ILicensingService
-dontnote com.google.vending.licensing.ILicensingService
-dontnote com.google.android.vending.licensing.ILicensingService

# For native methods, see http://proguard.sourceforge.net/manual/examples.html#native
-keepclasseswithmembernames class * {
    native <methods>;
}


# Keep setters in Views so that animations can still work.
-keepclassmembers public class * extends android.view.View {
    void set*(***);
    *** get*();
}

# We want to keep methods in Activity that could be used in the XML attribute onClick.
-keepclassmembers class * extends android.app.Activity {
    public void *(android.view.View);
}

# For enumeration classes, see http://proguard.sourceforge.net/manual/examples.html#enumerations
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

-keepclassmembers class * implements android.os.Parcelable {
    public static final ** CREATOR;
}

-keepclassmembers class **.R$* {
    public static <fields>;
}

# Preserve annotated Javascript interface methods.
-keepclassmembers class * {
    @android.webkit.JavascriptInterface <methods>;
}

# The support libraries contains references to newer platform versions.
# Don't warn about those in case this app is linking against an older
# platform version. We know about them, and they are safe.
-dontnote android.support.**
-dontnote androidx.**
-dontwarn android.support.**
-dontwarn androidx.**

# This class is deprecated, but remains for backward compatibility.
-dontwarn android.util.FloatMath

# Understand the @Keep support annotation.
-keep class androidx.annotation.Keep

-keep @androidx.annotation.Keep class * {*;}


-keepclasseswithmembers class * {
    @androidx.annotation.Keep <methods>;
}



-keepclasseswithmembers class * {
    @androidx.annotation.Keep <fields>;
}


-keepclasseswithmembers class * {
    @androidx.annotation.Keep <init>(...);
}

# These classes are duplicated between android.jar and org.apache.http.legacy.jar.
-dontnote org.apache.http.**
-dontnote android.net.http.**

-optimizations !code/simplification/arithmetic,!code/simplification/cast,!field/*,!class/merging/*
-optimizationpasses 5
-allowaccessmodification

-keep class com.google.android.material.** { *; }

-dontwarn com.google.android.material.**
-dontnote com.google.android.material.**
-dontwarn androidx.**
-keep class androidx.** { *; }
-keep interface androidx.** { *; }

#Retrofit
-dontwarn retrofit.**
-keep class retrofit.** { *; }
-keepattributes Signature
-keepattributes Exceptions

#okhttp3
-keepattributes Signature
-keepattributes *Annotation*
-keep class okhttp3.** { *; }
-keep interface okhttp3.** { *; }
-dontwarn okhttp3.**
-dontnote okhttp3.**

# Okio
-dontwarn java.nio.file.*
-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement
#Gson
-keep class com.google.gson.stream.** { *; }
#java
# Preserve all .class method names.

-keepclassmembernames class * {
    java.lang.Class class$(java.lang.String);
    java.lang.Class class$(java.lang.String, boolean);
}
 #Preserve all native method names and the names of their classes.

-keepclasseswithmembernames class * {
    native <methods>;
}

# Preserve the special static methods that are required in all enumeration
# classes.

-keepclassmembers class * extends java.lang.Enum {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

# Explicitly preserve all serialization members. The Serializable interface
# is only a marker interface, so it wouldn't save them.
# You can comment this out if your library doesn't use serialization.
# If your code contains serializable classes that have to be backward
# compatible, please refer to the manual.

-keepclassmembers class * implements java.io.Serializable {
    static final long serialVersionUID;
    static final java.io.ObjectStreamField[] serialPersistentFields;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}


-keep class com.appsbytes.allgodwallpapers.* {
  *;
}

-keep public class com.appsbytes.allgodwallpapers.activities.* {
	 public static final <fields>;
	 public <methods>;
}
-keep public class com.appsbytes.allgodwallpapers.application.* {
	 public static final <fields>;
	 public <methods>;
}
-keep public class com.appsbytes.allgodwallpapers.adapters.* {
	 public static final <fields>;
	 public <methods>;
}
-keep public class com.appsbytes.allgodwallpapers.api.* {
	 public static final <fields>;
	 public <methods>;
}
-keep public class com.appsbytes.allgodwallpapers.base.* {
	 public static final <fields>;
	 public <methods>;
}
-keep public class com.appsbytes.allgodwallpapers.firebaseclasses.* {
	 public static final <fields>;
	 public <methods>;
}
-keep public class com.appsbytes.allgodwallpapers.online.* {
	 public static final <fields>;
	 public <methods>;
}
-keep public class com.appsbytes.allgodwallpapers.model.* {
	 public static final <fields>;
	 public <methods>;
}
-keep public class com.appsbytes.allgodwallpapers.viewmodel.* {
	 public static final <fields>;
	 public <methods>;
}
-keep public class com.appsbytes.allgodwallpapers.temple.* {
	 public static final <fields>;
	 public <methods>;
}
-keep public class com.appsbytes.allgodwallpapers.recycler_click_listener.* {
	 public static final <fields>;
	 public <methods>;
}

