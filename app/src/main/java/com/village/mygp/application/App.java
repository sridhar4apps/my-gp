package com.village.mygp.application;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;

import com.google.firebase.analytics.FirebaseAnalytics;

public class App extends Application {

    private FirebaseAnalytics mFirebaseAnalytics;
    private static App instance;
    public void onCreate() {
        super.onCreate();
        instance = this;
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
    }

    public boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    public static App getInstance() {
        return instance;
    }
}
