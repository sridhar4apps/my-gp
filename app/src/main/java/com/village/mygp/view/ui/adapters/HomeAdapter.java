package com.village.mygp.view.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.village.mygp.R;
import com.village.mygp.model.Card;

public class HomeAdapter extends ListAdapter<Card, HomeAdapter.HomeViewHolder> {

    public ItemClickListener itemClickListener;

    public interface ItemClickListener {
        void itemClick(int i);
    }

    public HomeAdapter(DiffUtil.ItemCallback<Card> itemCallback, ItemClickListener itemClickListener2) {
        super(itemCallback);
        this.itemClickListener = itemClickListener2;
    }


    @NonNull
    @Override
    public HomeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new HomeViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.home_card_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull HomeViewHolder homeViewHolder, int position) {
        homeViewHolder.bind((Card) getItem(position));
    }

    class HomeViewHolder extends RecyclerView.ViewHolder {
        ImageView icon;
        TextView title;
        public HomeViewHolder(@NonNull View itemView) {
            super(itemView);
            this.title = (TextView) itemView.findViewById(R.id.title);
            this.icon = (ImageView) itemView.findViewById(R.id.icon);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemClickListener.itemClick(getAdapterPosition());
                }
            });
        }

        public void bind(Card card) {
            this.title.setText(card.getTitle());
            this.icon.setImageResource(card.getIcon());
        }
    }

}
