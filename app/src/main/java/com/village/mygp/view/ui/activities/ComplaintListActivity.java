package com.village.mygp.view.ui.activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DiffUtil;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;

import com.village.mygp.BuildConfig;
import com.village.mygp.R;
import com.village.mygp.application.App;
import com.village.mygp.databinding.ActivityComplaintListBinding;
import com.village.mygp.databinding.ComplaintReplayLayoutBinding;
import com.village.mygp.model.ComplaintListModel;
import com.village.mygp.model.ComplaintReplyModel;
import com.village.mygp.model.HelpLineSubModel;
import com.village.mygp.view.api.RetrofitApis;
import com.village.mygp.view.baseclasses.BaseActivity;
import com.village.mygp.view.baseclasses.ToastService;
import com.village.mygp.view.ui.adapters.ComplaintListAdapter;
import com.village.mygp.view.utilities.FlavorName;
import com.village.mygp.view.utilities.SharedPreferenceDataUtil;
import com.village.mygp.viewmodel.ComplaintListViewModel;
import com.village.mygp.viewmodel.LoginViewModel;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ComplaintListActivity extends BaseActivity implements ComplaintListAdapter.ItemClickListener {

    private ActivityComplaintListBinding binding;
    private SharedPreferenceDataUtil sharedPreferenceDataUtil;
    private ComplaintListViewModel complaintListViewModel;
    private ToastService toastService;
    private ComplaintListAdapter adapter;
    private ArrayList<ComplaintListModel.Result> arrayList = new ArrayList<>();

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(ComplaintListActivity.this, R.layout.activity_complaint_list);

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        height = displaymetrics.heightPixels;
        width = displaymetrics.widthPixels;

        this.toastService = new ToastService(this);
        sharedPreferenceDataUtil = new SharedPreferenceDataUtil(this);
        binding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        Bundle bundle = getIntent().getExtras();
        binding.typeName.setText(bundle.getString("types"));

        //complaintListViewModel   = new ViewModelProvider(this).get(ComplaintListViewModel.class);
        if (isNetworkAvailable()) {
            getComplaintList(sharedPreferenceDataUtil.getVillageId());
        } else {
            toastService.info("No Internet!");
        }

      /*  complaintListViewModel.getData().observe(ComplaintListActivity.this, new Observer<ComplaintListModel>() {
            @Override
            public void onChanged(ComplaintListModel qaModel) {
                if (qaModel.getStatusCode() != null && qaModel.getStatusCode() == 200) {
                    if (qaModel.getResult() != null && qaModel.getResult().size() > 0) {
                        binding.recyclerView.setVisibility(View.VISIBLE);
                        binding.empty.setVisibility(View.GONE);
                        arrayList.addAll(qaModel.getResult());
                        adapter.submitList(qaModel.getResult());
                        binding.recyclerView.setAdapter(adapter);
                    }
                }else{
                    binding.recyclerView.setVisibility(View.GONE);
                    binding.empty.setVisibility(View.VISIBLE);
                }
                hideCustomLoading();
            }
        });

        complaintListViewModel.getMessageToShow().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                toastService.danger(s);
                hideCustomLoading();

                binding.recyclerView.setVisibility(View.GONE);
                binding.empty.setVisibility(View.VISIBLE);
            }
        });*/
    }

    private void getComplaintList(String villageId) {
        showCustomLoading("");
        // complaintListViewModel.getVillageComplains(ComplaintListActivity.this,villageId);
        RetrofitApis retrofitApis = RetrofitApis.Factory.create();
        adapter = new ComplaintListAdapter((DiffUtil.ItemCallback<ComplaintListModel.Result>) null, this);
        Call<ComplaintListModel> modelCall = retrofitApis.getVillageComplaints(villageId);
        modelCall.enqueue(new Callback<ComplaintListModel>() {
            @Override
            public void onResponse(Call<ComplaintListModel> call, Response<ComplaintListModel> response) {
                if (response.isSuccessful()) {
                    ComplaintListModel qaModel = response.body();
                    if (qaModel.getStatusCode() != null && qaModel.getStatusCode() == 200) {
                        if (qaModel.getResult() != null && qaModel.getResult().size() > 0) {
                            binding.recyclerView.setVisibility(View.VISIBLE);
                            binding.empty.setVisibility(View.GONE);
                            arrayList.addAll(qaModel.getResult());
                            adapter.submitList(qaModel.getResult());
                            binding.recyclerView.setAdapter(adapter);
                        }
                    } else {
                        binding.recyclerView.setVisibility(View.GONE);
                        binding.empty.setVisibility(View.VISIBLE);
                    }
                } else {
                    showToastMessage(response.message());
                }
                hideCustomLoading();
            }

            @Override
            public void onFailure(Call<ComplaintListModel> call, Throwable t) {
                if (App.getInstance().isNetworkAvailable(ComplaintListActivity.this)) {
                    showToastMessage("Something went wrong! please contact your administrator");
                } else {
                    showToastMessage("Please check your internet connection!");
                }

                hideCustomLoading();
            }
        });
    }

    @Override
    public void itemClick(int i) {
        if (FlavorName.SARPANCH.name().equalsIgnoreCase(BuildConfig.FLAVOR)) {
            if (arrayList.get(i).getCurrentStatus().equals("1")) {

                Log.e("msg1", "" + arrayList.get(i).getCurrentStatus());
            } else {

                Log.e("msg2", "" + arrayList.get(i).getCurrentStatus());
                replySubmit(arrayList.get(i).getComplaintId(), arrayList.get(i).getUserDescription());
            }
        }

    }


    public static int height, width;
    Dialog dialog1;
    private String statusValue;
    ComplaintReplayLayoutBinding replayLayoutBinding;

    private void replySubmit(String complaintId, String userDescription) {
        dialog1 = new Dialog(ComplaintListActivity.this);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog1.setCancelable(false);
        // dialog1.setCanceledOnTouchOutside(false);
        replayLayoutBinding = ComplaintReplayLayoutBinding.inflate(getLayoutInflater());
        dialog1.setContentView(replayLayoutBinding.getRoot());
        //dialog1.getWindow().setLayout(width - width / 8, height - height / 2);
        replayLayoutBinding.complaintName.setText("Complaint : " + userDescription);
        replayLayoutBinding.btnSubmit.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                if (validation()) {
                    if (isNetworkAvailable()) {
                        submitReplay(replayLayoutBinding.etAddress.getText().toString(), statusValue, complaintId);

                        dialog1.dismiss();

                    } else {
                        toastService.info("No Internet!");
                        dialog1.dismiss();
                    }
                }

            }
        });

        replayLayoutBinding.spinnerStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 1) {
                    statusValue = "1";
                } else {
                    statusValue = "0";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        dialog1.show();
        dialog1.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }

    private void submitReplay(String repsonseData, String statusValue, String complaintId) {
        Log.e("msgss", "" + statusValue);
        showCustomLoading("");
        RetrofitApis retrofitApis = RetrofitApis.Factory.create();
        Call<ComplaintReplyModel> modelCall = retrofitApis.complaintReplay(complaintId, sharedPreferenceDataUtil.getVillageId(), repsonseData, statusValue);
        modelCall.enqueue(new Callback<ComplaintReplyModel>() {
            @Override
            public void onResponse(Call<ComplaintReplyModel> call, Response<ComplaintReplyModel> response) {
                if (response.isSuccessful()) {
                    ComplaintReplyModel helpLineSubModel = response.body();
                    if (helpLineSubModel.getStatusCode() != null && helpLineSubModel.getStatusCode() == 200) {

                        showToastMessage(helpLineSubModel.getStatusMessage());
                        getComplaintList(sharedPreferenceDataUtil.getVillageId());
                    } else {
                        showToastMessage(helpLineSubModel.getStatusMessage());
                    }

                } else {

                    showToastMessage(response.message());
                }
                //hideCustomLoading();
            }

            @Override
            public void onFailure(Call<ComplaintReplyModel> call, Throwable t) {
                if (App.getInstance().isNetworkAvailable(ComplaintListActivity.this)) {
                    showToastMessage("Something went wrong! please contact your administrator");
                } else {
                    showToastMessage("Please check your internet connection!");
                }
                hideCustomLoading();
            }
        });
    }

    private boolean validation() {
        if (TextUtils.isEmpty(replayLayoutBinding.etAddress.getText().toString())) {
            replayLayoutBinding.etAddress.requestFocus();
            showToastMessage("Please Enter Response");
            return false;
        }
        if (replayLayoutBinding.spinnerStatus.getSelectedItemPosition() == 0 || replayLayoutBinding.spinnerStatus.getSelectedItem() == null) {
            showToastMessage("Select Status");
            return false;
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();

    }
}