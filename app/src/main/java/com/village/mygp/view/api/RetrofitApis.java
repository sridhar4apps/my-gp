package com.village.mygp.view.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

import com.village.mygp.BuildConfig;
import com.village.mygp.model.BannerModel;
import com.village.mygp.model.ComplaintListModel;
import com.village.mygp.model.ComplaintModel;
import com.village.mygp.model.ComplaintReplyModel;
import com.village.mygp.model.DistrictModel;
import com.village.mygp.model.GalleryModel;
import com.village.mygp.model.HelpLineModel;
import com.village.mygp.model.HelpLineSubModel;
import com.village.mygp.model.HelplineDeletModel;
import com.village.mygp.model.MandalModel;
import com.village.mygp.model.MessageModel;
import com.village.mygp.model.SarpanchModel;
import com.village.mygp.model.StateModel;
import com.village.mygp.model.TokenModel;
import com.village.mygp.model.VillageModel;

public interface RetrofitApis {

    class Factory {
        public static RetrofitApis create() {
            //create logger
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
           /* if()
            logging.setLevel(HttpLoggingInterceptor.Level.HEADERS);
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);*/
            if (BuildConfig.BUILD_TYPE.contentEquals("debug")) {
                logging.setLevel(HttpLoggingInterceptor.Level.HEADERS);
                logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            } else {
                logging.setLevel(HttpLoggingInterceptor.Level.NONE);
            }


            // default time out is 15 seconds
            OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .addInterceptor(logging)
                    .build();

            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BuildConfig.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();
            return retrofit.create(RetrofitApis.class);
        }
    }


    @FormUrlEncoded
    @POST("political/JSON/login_customer.php")
    Call<SarpanchModel> getSarpanchLogin(@Field("mobile") String mobileNumber,
                                         @Field("password") String emailId);

    @FormUrlEncoded
    @POST("political/JSON/complaints_list.php")
    Call<ComplaintListModel> getVillageComplaints(@Field("village_id") String vId);



    @POST("political/JSON/documents_list.php")
    Call<GalleryModel> getGalleryData(@Query("village_id") String vId,
                                      @Query("category_id") String category_id);


    @FormUrlEncoded
    @POST("political/JSON/complaint_create.php")
    Call<ComplaintModel> complaintSubmit(@Field("user_id") String user_id,
                                         @Field("village_id") String vId,
                                         @Field("category_id") String category_id,
                                         @Field("name") String name,
                                         @Field("mobile") String mobile,
                                         @Field("ward_no") String ward_no,
                                         @Field("address") String address,
                                         @Field("description") String description,
                                         @Field("device_id") String device_id);


    @Multipart
    @POST("political/JSON/documents_upload.php")
    Call<MessageModel> messageSubmit(@Part("user_id") RequestBody user_id,
                                     @Part("village_id") RequestBody vId,
                                     @Part("description") RequestBody description,
                                     @Part("type") RequestBody typeId,
                                     @Part("category_id") RequestBody category_id,
                                     @Part("device_id") RequestBody device_id,
                                     @Part MultipartBody.Part documentUpload);


    @GET("political/JSON/statelist.php")
    Call<StateModel> getStateData();


    @GET("political/JSON/district_list.php")
    Call<DistrictModel> getDistrictData();


    @POST("political/JSON/mandal_list.php")
    Call<MandalModel> getMandalData(@Query("districtid") String vId);


    @POST("political/JSON/village_list.php")
    Call<VillageModel> getVillageData(@Query("city_id") String vId);


    @POST("political/JSON/getbanners.php")
    Call<BannerModel> getBannerData(@Query("village_id") String vId);


    @POST("political/JSON/helplines_list.php")
    Call<HelpLineModel> getHelpLineData(@Query("village_id") String vId);


    @FormUrlEncoded
    @POST("political/JSON/update_user_device.php")
    Call<TokenModel> tokenSave(@Field("device_id") String device_id,
                               @Field("village_id") String vId,
                               @Field("user_type") String type,
                               @Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("political/JSON/complaint_update.php")
    Call<ComplaintReplyModel> complaintReplay(@Field("complaint_id") String complaintId,
                                              @Field("village_id") String vId,
                                              @Field("response") String responseTxt,
                                              @Field("status") String statusId);


    @FormUrlEncoded
    @POST("political/JSON/helplines_delete.php")
    Call<HelplineDeletModel> helpLineDelete(@Field("village_id") String vId,
                                            @Field("helpline_id") String hId);


    @FormUrlEncoded
    @POST("political/JSON/helplines_create.php")
    Call<HelpLineSubModel> helpLineSubmit(@Field("village_id") String vId,
                                          @Field("designation") String designation,
                                          @Field("number") String number);

}
