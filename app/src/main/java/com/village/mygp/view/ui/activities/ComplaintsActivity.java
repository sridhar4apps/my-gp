package com.village.mygp.view.ui.activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.village.mygp.R;
import com.village.mygp.databinding.ActivityComplaintsBinding;
import com.village.mygp.model.ComplaintListModel;
import com.village.mygp.model.ComplaintModel;
import com.village.mygp.view.baseclasses.BaseActivity;
import com.village.mygp.view.baseclasses.ToastService;
import com.village.mygp.view.ui.dialog.SingleButtonAlertDialog;
import com.village.mygp.view.utilities.SharedPreferenceDataUtil;
import com.village.mygp.view.utilities.UtilManager;
import com.village.mygp.viewmodel.ComplaintViewModel;

public class ComplaintsActivity extends BaseActivity {


    private ActivityComplaintsBinding binding;
    private String catValue;
    private SharedPreferenceDataUtil sharedPreferenceDataUtil;
    private ToastService toastService;
    private ComplaintViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(ComplaintsActivity.this, R.layout.activity_complaints);
        toastService = new ToastService(this);
        sharedPreferenceDataUtil = new SharedPreferenceDataUtil(this);
        viewModel = new ViewModelProvider(this).get(ComplaintViewModel.class);
        binding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        Bundle bundle = getIntent().getExtras();
        binding.complainName.setText(bundle.getString("department"));
        binding.image.setImageResource(bundle.getInt("12345"));
        catValue = String.valueOf(bundle.getInt("category"));

        binding.btnSubmit.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                if (isNetworkAvailable()) {
                    if (validation()) {

                        if (isNetworkAvailable()) {
                            submitComplaintData();

                        } else {
                            toastService.info("No Internet!");
                        }
                    }
                } else {
                    toastService.info("No Internet!");
                }
            }
        });
        viewModel.getData().observe(ComplaintsActivity.this, new Observer<ComplaintModel>() {
            @Override
            public void onChanged(ComplaintModel qaModel) {
                if (qaModel.getStatusCode() != null && qaModel.getStatusCode() == 200) {
                    UtilManager.showErrorMessage(qaModel.getStatusMessage(), ComplaintsActivity.this, new SingleButtonAlertDialog.ButtonClick() {
                        @Override
                        public void onClick(View v) {

                            finish();
                        }
                    });
                } else {

                }
                hideCustomLoading();
            }
        });

        viewModel.getMessageToShow().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                UtilManager.showErrorMessage(s, ComplaintsActivity.this, new SingleButtonAlertDialog.ButtonClick() {
                    @Override
                    public void onClick(View v) {

                        finish();
                    }
                });
                hideCustomLoading();

            }
        });

    }

    private void submitComplaintData() {

        showCustomLoading("");
        viewModel.submitData(ComplaintsActivity.this, sharedPreferenceDataUtil.getUserId(), sharedPreferenceDataUtil.getVillageId(), catValue, binding.etName.getText().toString().trim(),
                binding.etMobile.getText().toString().trim(), binding.etWard.getText().toString().trim(), binding.etAddress.getText().toString().trim(), binding.etComplaint.getText().toString().trim(),getPreferToken(SharedPreferenceDataUtil.refreshedToken));
    }

    private boolean validation() {
        if (TextUtils.isEmpty(binding.etName.getText().toString())) {
            binding.etName.requestFocus();
            showToastMessage("Please Enter Name");
            return false;
        }
        if (!validatePhNo(getApplicationContext(), binding.etMobile)) {
            binding.etMobile.requestFocus();
            return false;
        }
        if (TextUtils.isEmpty(binding.etWard.getText().toString())) {
            binding.etWard.requestFocus();
            showToastMessage("Please Enter ward");
            return false;
        }
        if (TextUtils.isEmpty(binding.etAddress.getText().toString())) {
            binding.etAddress.requestFocus();
            showToastMessage("Please Enter Address");
            return false;
        }
        if (TextUtils.isEmpty(binding.etComplaint.getText().toString())) {
            binding.etComplaint.requestFocus();
            showToastMessage("Please Enter Complaint");
            return false;
        }
        return true;
    }
}