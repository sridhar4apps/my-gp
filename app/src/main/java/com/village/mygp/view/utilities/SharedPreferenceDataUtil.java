package com.village.mygp.view.utilities;

import static android.content.Context.MODE_PRIVATE;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.io.IOException;
import java.security.GeneralSecurityException;

public class SharedPreferenceDataUtil {

    private SharedPreferences prefs;
    private static final String TAG = SharedPreferenceDataUtil.class.getSimpleName();
    static String key = null;
    public static final String MY_PREFERENCES = "myPreferences";
    public static final String MY_PREFERENCES1 = "myPreferencesToken";

    public SharedPreferenceDataUtil(Context context) {
        prefs = context.getSharedPreferences(MY_PREFERENCES, MODE_PRIVATE);
        //this.prefs = PreferenceManager.getDefaultSharedPreferences(context);
    }



    public static final String LANGUAGE = "language";
    public static final String COUNTRY = "country";
    public static final String STATE_NAME = "STATE_NAME";
    public static final String STATE_ID = "STATE_ID";
    public static final String DISTRICT_NAME = "DISTRICT_NAME";
    public static final String DISTRICT_ID = "DISTRICT_ID";
    public static final String MANDAL_ID = "MANDAL_ID";
    public static final String MANDAL_NAME = "MANDAL_NAME";
    public static final String VILLAGE_ID = "VILLAGE_ID";
    public static final String VILLAGE_NAME = "VILLAGE_NAME";
    public static final String USER_ID = "USER_ID";
    public static final String IMAGE_PATH = "IMAGE_PATH";
    public static final String USER_THEME = "USER_THEME";
    public static final String refreshedToken = "refreshedToken";

    public void setNotifyToken(String value) {
        setPreference(refreshedToken, value);
    }

    public String getNotifyToken() {
        return prefs.getString(refreshedToken, "");
    }

    public void setAppTheme(String name)
    {
        setPreference(USER_THEME,name);
    }

    public AppThemes getAppTheme(){
        return AppThemes.valueOf(prefs.getString(USER_THEME,AppThemes.SAME.name()));
    }


    public void setMandalId(String value) {
        setPreference(MANDAL_ID, value);
    }

    public String getMandalId() {
        return prefs.getString(MANDAL_ID, "");
    }

    public void setDistrictId(String value) {
        setPreference(DISTRICT_ID, value);
    }

    public String getDistrictId() {
        return prefs.getString(DISTRICT_ID, "");
    }

    public void setStateId(String value) {
        setPreference(STATE_ID, value);
    }

    public String getStateId() {
        return prefs.getString(STATE_ID, "");
    }

    public void setImagePath(String value) {
        setPreference(IMAGE_PATH, value);
    }

    public String getImagePath() {
        return prefs.getString(IMAGE_PATH, "");
    }

    public static final String location_select = "location_select";
    public static final String languageSelect = "languageSelect";
    public boolean getLanguageSelect() {
        return prefs.getBoolean(languageSelect, false);
    }

    public void setLanguageSelect(boolean key) {
        setPreference(languageSelect, key);
    }


    public String getStateName() {
        return prefs.getString(STATE_NAME, "");
    }

    public void setStateName(String value) {
        setPreference(STATE_NAME, value);
    }

    public String getDistrictName() {
        return prefs.getString(DISTRICT_NAME, "");
    }

    public void setDistrictName(String value) {
        setPreference(DISTRICT_NAME, value);
    }

    public String getMandalName() {
        return prefs.getString(MANDAL_NAME, "");
    }

    public void setMandalName(String value) {
        setPreference(MANDAL_NAME, value);
    }

    public String getVillageName() {
        return prefs.getString(VILLAGE_NAME, "");
    }

    public void setVillageName(String value) {
        setPreference(VILLAGE_NAME, value);
    }

    public boolean getLocationSelect() {
        return prefs.getBoolean(location_select, false);
    }

    public void settUserId(String value) {
        setPreference(USER_ID, value);
    }

    public String getUserId() {
        return prefs.getString(USER_ID, "");
    }

    public void setLocationSelect(boolean key) {
        setPreference(location_select, key);
    }

    public void setVillageId(String value) {
        setPreference(VILLAGE_ID, value);
    }

    public String getVillageId() {
        return prefs.getString(VILLAGE_ID, "");
    }
    public void setSelectedLanguage(String language, String country) {
        setPreference(LANGUAGE, language);
        setPreference(COUNTRY, country);
    }
    @Deprecated
    public boolean commit() {
        return true;
    }
    public String getSelectedLanguage() {
        return prefs.getString(LANGUAGE, "en");
    }
    private void setPreference(String key, String value) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.apply();
    }

    private void setPreference(String key, boolean value) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    private void setPreference(String key, int value) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    private void setPreference(String key, float value) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putFloat(key, value);
        editor.apply();
    }

    private void setPreference(String key, long value) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putLong(key, value);
        editor.apply();
    }

    private void removePreference(String key) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(key);
        editor.apply();
    }


    public void clearAllPreferences(Context context) {
            SharedPreferences.Editor editor = prefs.edit();
            editor.clear();
            editor.apply();
            editor.commit();

    }
}
