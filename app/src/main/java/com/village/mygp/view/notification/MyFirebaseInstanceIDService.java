package com.village.mygp.view.notification;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.village.mygp.BuildConfig;
import com.village.mygp.R;
import com.village.mygp.view.ui.activities.ComplaintListActivity;
import com.village.mygp.view.ui.activities.GalleryActivity;
import com.village.mygp.view.ui.activities.MainActivity;
import com.village.mygp.view.ui.activities.MessageActivity;
import com.village.mygp.view.utilities.FlavorName;
import com.village.mygp.view.utilities.SharedPreferenceDataUtil;

public class MyFirebaseInstanceIDService extends FirebaseMessagingService {

    private SharedPreferenceDataUtil sharedPreferenceDataUtil;

    @Override
    public void onCreate() {
        super.onCreate();
       // sharedPreferenceDataUtil = new SharedPreferenceDataUtil(this);

    }

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        setPreferToken(s);
        //sharedPreferenceDataUtil.setNotifyToken(s);

        Log.e("msgs", "" + s);
    }

    private void setPreferToken(String s) {
        SharedPreferences preferences = this.getApplicationContext().getSharedPreferences(SharedPreferenceDataUtil.MY_PREFERENCES1, MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(SharedPreferenceDataUtil.refreshedToken, s);
        editor.apply();
    }

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.d("msgs", "From: " + remoteMessage.getFrom());
        Log.d("msgs", "Message data payload: " + remoteMessage.getData());
        if (remoteMessage.getData().size() > 0) {
            Log.d("msgs", "Message data payload: " + remoteMessage.getData());

            String message = remoteMessage.getData().get("message");
            String title = remoteMessage.getData().get("title");
            String img_link = remoteMessage.getData().get("image");
            String types = remoteMessage.getData().get("type");
            String village_id = remoteMessage.getData().get("village_id");
            String user_id = remoteMessage.getData().get("user_id");
            Log.e("data", "" + message + "  " + title + " " + types + "  " + img_link);
            sendNotification(message, title, types, img_link);

        }

        if (remoteMessage.getNotification() != null) {
            Log.d("msgs", "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }
    }

    private void sendNotification(String messageBody, String title, String types, String img_link) {
        MediaPlayer mp = MediaPlayer.create(this, R.raw.notification);
        mp.start();
        Intent intent = null;
        Bundle bundle = new Bundle();

        if (FlavorName.SARPANCH.name().equalsIgnoreCase(BuildConfig.FLAVOR) || FlavorName.TRSSARPANCH.name().equalsIgnoreCase(BuildConfig.FLAVOR)) {
            if(types.equals("1"))
            {
                bundle.putString("types", "ఫిర్యాదులు");
                bundle.putInt("value", 1);
                intent = new Intent(this, ComplaintListActivity.class);
                intent.putExtras(bundle);
            }
            if(types.equals("2"))
            {
                bundle.putString("types", "సందేశాలు");
                bundle.putInt("value", 1);
                intent = new Intent(this, MessageActivity.class);
                intent.putExtras(bundle);
            }
            if(types.equals("3"))
            {
                bundle.putString("types", "సంక్షేమ కార్యక్రమాలు");
                bundle.putInt("value", 2);
                intent = new Intent(this, MessageActivity.class);
                intent.putExtras(bundle);
            }
        }else{
            if(types.equals("1"))
            {
                bundle.putString("types", "ఫిర్యాదులు");
                bundle.putInt("value", 1);
                intent = new Intent(this, ComplaintListActivity.class);
                intent.putExtras(bundle);
            }
            if(types.equals("2"))
            {
                bundle.putString("types", "సందేశాలు");
                bundle.putInt("value", 1);
                intent = new Intent(this, GalleryActivity.class);
                intent.putExtras(bundle);
            }
            if(types.equals("3"))
            {
                bundle.putString("types", "సంక్షేమ కార్యక్రమాలు");
                bundle.putInt("value", 2);
                intent = new Intent(this, GalleryActivity.class);
                intent.putExtras(bundle);
            }
        }


        //intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            pendingIntent = PendingIntent.getActivity(this,
                    0, intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_MUTABLE | PendingIntent.FLAG_ONE_SHOT);

        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            pendingIntent = PendingIntent.getActivity(this,
                    0, intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE | PendingIntent.FLAG_ONE_SHOT);
        } else {
            pendingIntent = PendingIntent.getActivity(this,
                    0, intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);

        }

        String channelId = getString(R.string.default_notification_channel_id);
        //Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.mipmap.ic_launcher_round)
                        .setLargeIcon(BitmapFactory.decodeResource(this.getResources(),
                                R.mipmap.ic_launcher_round))
                        .setContentTitle(title)
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        //.setSound(defaultSoundUri)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify((int) System.currentTimeMillis(), notificationBuilder.build());
    }
}
