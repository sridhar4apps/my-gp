package com.village.mygp.view.ui.activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;

import com.village.mygp.BuildConfig;
import com.village.mygp.R;
import com.village.mygp.application.App;
import com.village.mygp.databinding.ActivitySplashBinding;
import com.village.mygp.view.baseclasses.BaseActivity;
import com.village.mygp.view.baseclasses.ToastService;
import com.village.mygp.view.utilities.FlavorName;
import com.village.mygp.view.utilities.SharedPreferenceDataUtil;

@SuppressLint("CustomSplashScreen")
public class SplashActivity extends BaseActivity {

    private ToastService toastService;
    private ActivitySplashBinding binding;
    private SharedPreferenceDataUtil sharedPreferenceDataUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        //setContentView(R.layout.activity_splash);
        binding = DataBindingUtil.setContentView(SplashActivity.this, R.layout.activity_splash);
        binding.animationViewView.playAnimation();
        sharedPreferenceDataUtil = new SharedPreferenceDataUtil(this);

        this.toastService = new ToastService(this);
        if (App.getInstance().isNetworkAvailable(this)) {
            new Handler().postDelayed(() -> {
                if(sharedPreferenceDataUtil.getLanguageSelect())
                {
                    Intent build;
                    if (FlavorName.SARPANCH.name().equalsIgnoreCase(BuildConfig.FLAVOR) || FlavorName.TRSSARPANCH.name().equalsIgnoreCase(BuildConfig.FLAVOR)) {
                        if(sharedPreferenceDataUtil.getLocationSelect())
                        {
                            build = MainActivity.build(SplashActivity.this);
                        }else{

                            build = SarpanchLoginActivity.build(SplashActivity.this);
                        }
                    } else {
                        if(sharedPreferenceDataUtil.getLocationSelect())
                        {
                            build = MainActivity.build(SplashActivity.this);
                        }else{

                            build = LocalityActivity.build(SplashActivity.this);
                        }
                    }
                    startActivity(build);
                }else{
                    Intent intent = LanguageActivity.build(SplashActivity.this, "languages");
                    startActivity(intent);
                }
                finish();


            }, 3000);
        }

    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onResume() {
        super.onResume();
        if (!App.getInstance().isNetworkAvailable(this)) {
            toastService.info("No Internet!");
        }
    }
}