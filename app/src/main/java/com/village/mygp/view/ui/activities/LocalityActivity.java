package com.village.mygp.view.ui.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.village.mygp.BuildConfig;
import com.village.mygp.R;
import com.village.mygp.application.App;
import com.village.mygp.databinding.ActivityLocalityBinding;
import com.village.mygp.model.DistrictModel;
import com.village.mygp.model.MandalModel;
import com.village.mygp.model.StateModel;
import com.village.mygp.model.TokenModel;
import com.village.mygp.model.VillageModel;
import com.village.mygp.view.api.RetrofitApis;
import com.village.mygp.view.baseclasses.BaseActivity;
import com.village.mygp.view.utilities.FlavorName;
import com.village.mygp.view.utilities.SharedPreferenceDataUtil;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LocalityActivity extends BaseActivity implements AdapterView.OnItemSelectedListener {
    public static Intent build(Activity activity) {
        Intent intent = new Intent(activity, LocalityActivity.class);
        return intent;
    }

    private SharedPreferenceDataUtil sharedPreferenceDataUtil;

    private ActivityLocalityBinding binding;
    private RetrofitApis retrofitApis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(LocalityActivity.this, R.layout.activity_locality);
        sharedPreferenceDataUtil = new SharedPreferenceDataUtil(this);

        retrofitApis = RetrofitApis.Factory.create();
        loadState();

        binding.btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validation()) {
                    sharedPreferenceDataUtil.setLocationSelect(true);
                    sharedPreferenceDataUtil.setVillageId(villageId);
                    sharedPreferenceDataUtil.setVillageName(villageName);
                    sharedPreferenceDataUtil.setMandalId(mandalId);
                    sharedPreferenceDataUtil.setDistrictId(districtId);
                    sharedPreferenceDataUtil.setStateId(stateId);
                    sharedPreferenceDataUtil.setLanguageSelect(true);
                    //sharedPreferenceDataUtil.setAppTheme("TDP");
                    if (FlavorName.SARPANCH.name().equalsIgnoreCase(BuildConfig.FLAVOR) || FlavorName.TRSSARPANCH.name().equalsIgnoreCase(BuildConfig.FLAVOR)) {
                        saveToken(getPreferToken(SharedPreferenceDataUtil.refreshedToken),sharedPreferenceDataUtil.getVillageId(),"1",sharedPreferenceDataUtil.getUserId());
                    }else{
                        saveToken(getPreferToken(SharedPreferenceDataUtil.refreshedToken),sharedPreferenceDataUtil.getVillageId(),"2","");

                    }

                }
            }
        });

        binding.spinnerState.setOnItemSelectedListener(this);
        binding.spinnerDistrict.setOnItemSelectedListener(this);
        binding.spinnerMandal.setOnItemSelectedListener(this);
        binding.spinnerVillage.setOnItemSelectedListener(this);
    }

    private void saveToken(String notifyToken, String villageId, String userType, String userId) {
        showCustomLoading("");
        RetrofitApis retrofitApis = RetrofitApis.Factory.create();
        Call<TokenModel> tokenModelCall = retrofitApis.tokenSave(notifyToken,villageId,userType,userId);
        tokenModelCall.enqueue(new Callback<TokenModel>() {
            @Override
            public void onResponse(Call<TokenModel> call, Response<TokenModel> response) {
                if(response.isSuccessful())
                {
                    TokenModel model = response.body();
                    if(model.getSuccess().equalsIgnoreCase("true"))
                    {
                        Intent build = MainActivity.build(LocalityActivity.this);
                        startActivity(build);
                        finish();
                    }
                    showToastMessage(model.getMessage());
                }else{
                    showToastMessage(response.message());
                }
                hideCustomLoading();
            }

            @Override
            public void onFailure(Call<TokenModel> call, Throwable t) {
                hideCustomLoading();
            }
        });
    }

    private ArrayList<String> stateNameArray = new ArrayList<>();
    private ArrayList<String> stateIdArray = new ArrayList<>();
    private ArrayAdapter<String> stateAdapter;
    private String stateId;

    private void loadState() {
        showCustomLoading("");
        Call<StateModel> modelCall = retrofitApis.getStateData();
        modelCall.enqueue(new Callback<StateModel>() {
            @Override
            public void onResponse(Call<StateModel> call, Response<StateModel> response) {
                if (response.isSuccessful()) {
                    StateModel model = response.body();
                    if (model != null && model.getStatusCode() == 200) {
                        if (model.getResult() != null && model.getResult().size() > 0) {
                            stateNameArray.clear();
                            stateIdArray.clear();
                            stateNameArray.add(0, "--Select--");
                            stateIdArray.add(0, "0");
                            for (int i = 0; i < model.getResult().size(); i++) {
                                stateNameArray.add(model.getResult().get(i).getStateTitle());
                                stateIdArray.add(model.getResult().get(i).getStateId());
                            }
                            stateAdapter = new ArrayAdapter<String>(LocalityActivity.this,
                                    android.R.layout.simple_dropdown_item_1line, stateNameArray);
                            binding.spinnerState.setAdapter(stateAdapter);

                        }
                    } else {
                        showToastMessage(model.getStatusMessage());
                    }
                } else {
                    showToastMessage(response.message());
                }
                hideCustomLoading();

            }

            @Override
            public void onFailure(Call<StateModel> call, Throwable t) {
                if (App.getInstance().isNetworkAvailable(LocalityActivity.this)) {
                    showToastMessage("Something went wrong! please contact your administrator");
                } else {
                    showToastMessage("Please check your internet connection!");
                }

                hideCustomLoading();
            }
        });

        /*ArrayAdapter<String> spinnerStateArrayAdapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_dropdown_item_1line,
                getResources().getStringArray(R.array.states));
        binding.spinnerState.setAdapter(spinnerStateArrayAdapter);*/
    }

    private boolean validation() {
        if (binding.spinnerState.getSelectedItemPosition() == 0 || binding.spinnerState.getSelectedItem() == null) {
            showToastMessage("Select State");
            return false;
        }

        if (binding.spinnerDistrict.getSelectedItemPosition() == 0 || binding.spinnerDistrict.getSelectedItem() == null) {
            showToastMessage("Select District");
            return false;
        }

        if (binding.spinnerMandal.getSelectedItemPosition() == 0 || binding.spinnerMandal.getSelectedItem() == null) {
            showToastMessage("Select Mandal");
            return false;
        }
        if (binding.spinnerVillage.getSelectedItemPosition() == 0 || binding.spinnerVillage.getSelectedItem() == null) {
            showToastMessage("Select Village");
            return false;
        }

        return true;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        parent.setSelection(position);
        Spinner spinner = (Spinner) parent;
        switch (parent.getId()) {
            case R.id.spinner_state:
                stateId = stateIdArray.get(position);
                Log.e("stateId ", stateId);
                if (isNetworkAvailable()) {
                    loadDistrict();
                } else {
                    showToastMessage("Please Check The Internet Connection");
                }

                break;
            case R.id.spinner_district:
                districtId = districtIdArray.get(position);
                Log.e("districtId ", districtId);
                if (isNetworkAvailable()) {
                    loadMandal(districtId);
                } else {
                    showToastMessage("Please Check The Internet Connection");
                }

                break;
            case R.id.spinner_mandal:
                mandalId = mandalIdArray.get(position);
                Log.e("mandalId ", mandalId);
                if (isNetworkAvailable()) {
                    loadVillage(mandalId);
                } else {
                    showToastMessage("Please Check The Internet Connection");
                }

                break;

            case R.id.spinner_village:
                villageId = villageIdArray.get(position);
                villageName = villageNameArray.get(position);
                Log.e("mandalId ", villageId + "  " + villageName);
              /*  if (isNetworkAvailable()){
                    loadDistrict();
                }else {
                    showToastMessage("Please Check The Internet Connection");
                }*/

                break;
        }


    }

    private ArrayList<String> villageNameArray = new ArrayList<>();
    private ArrayList<String> villageIdArray = new ArrayList<>();
    private ArrayAdapter<String> villageAdapter;
    private String villageId, villageName;

    private void loadVillage(String mandalId) {
        showCustomLoading("");
        String city_id = "" + mandalId;
        Call<VillageModel> modelCall = retrofitApis.getVillageData(city_id);
        modelCall.enqueue(new Callback<VillageModel>() {
            @Override
            public void onResponse(@NonNull Call<VillageModel> call, @NonNull Response<VillageModel> response) {
                if (response.isSuccessful()) {
                    VillageModel model = response.body();
                    if (model != null && model.getStatusCode() == 200) {
                        if (model.getResult() != null && model.getResult().size() > 0) {
                            villageNameArray.clear();
                            villageIdArray.clear();

                            villageNameArray.add(0, "--Select--");
                            villageIdArray.add(0, "0");
                            for (int i = 0; i < model.getResult().size(); i++) {
                                villageNameArray.add(model.getResult().get(i).getAreaName());
                                villageIdArray.add(model.getResult().get(i).getAreaId());
                            }
                            villageAdapter = new ArrayAdapter<String>(LocalityActivity.this,
                                    android.R.layout.simple_dropdown_item_1line, villageNameArray);
                            binding.spinnerVillage.setAdapter(villageAdapter);

                        }
                    } else {
                        assert model != null;
                        showToastMessage(model.getStatusMessage());
                    }
                } else {
                    showToastMessage(response.message());
                }
                hideCustomLoading();

            }

            @Override
            public void onFailure(@NonNull Call<VillageModel> call, @NonNull Throwable t) {
                if (App.getInstance().isNetworkAvailable(LocalityActivity.this)) {
                    showToastMessage("Something went wrong! please contact your administrator");
                } else {
                    showToastMessage("Please check your internet connection!");
                }

                hideCustomLoading();
            }
        });

    }

    private ArrayList<String> mandalNameArray = new ArrayList<>();
    private ArrayList<String> mandalIdArray = new ArrayList<>();
    private ArrayAdapter<String> mandalAdapter;
    private String mandalId;

    private void loadMandal(String districtId) {
        showCustomLoading("");
        String districtid = "" + districtId;

        Call<MandalModel> modelCall = retrofitApis.getMandalData(districtid);
        modelCall.enqueue(new Callback<MandalModel>() {
            @Override
            public void onResponse(@NonNull Call<MandalModel> call, @NonNull Response<MandalModel> response) {
                if (response.isSuccessful()) {
                    MandalModel model = response.body();
                    if (model != null && model.getStatusCode() == 200) {
                        if (model.getResult() != null && model.getResult().size() > 0) {
                            mandalNameArray.clear();
                            mandalIdArray.clear();

                            mandalNameArray.add(0, "--Select--");
                            mandalIdArray.add(0, "0");
                            for (int i = 0; i < model.getResult().size(); i++) {
                                mandalNameArray.add(model.getResult().get(i).getName());
                                mandalIdArray.add(model.getResult().get(i).getId());
                            }
                            mandalAdapter = new ArrayAdapter<String>(LocalityActivity.this,
                                    android.R.layout.simple_dropdown_item_1line, mandalNameArray);
                            binding.spinnerMandal.setAdapter(mandalAdapter);

                        }
                    } else {
                        assert model != null;
                        showToastMessage(model.getStatusMessage());
                    }
                } else {
                    showToastMessage(response.message());
                }
                hideCustomLoading();

            }

            @Override
            public void onFailure(@NonNull Call<MandalModel> call, @NonNull Throwable t) {
                if (App.getInstance().isNetworkAvailable(LocalityActivity.this)) {
                    showToastMessage("Something went wrong! please contact your administrator");
                } else {
                    showToastMessage("Please check your internet connection!");
                }

                hideCustomLoading();
            }
        });
    }


    private ArrayList<String> districtNameArray = new ArrayList<>();
    private ArrayList<String> districtIdArray = new ArrayList<>();
    private ArrayAdapter<String> districtAdapter;
    private String districtId;

    private void loadDistrict() {
        showCustomLoading("");
        Call<DistrictModel> modelCall = retrofitApis.getDistrictData();
        modelCall.enqueue(new Callback<DistrictModel>() {
            @Override
            public void onResponse(@NonNull Call<DistrictModel> call, @NonNull Response<DistrictModel> response) {
                if (response.isSuccessful()) {
                    DistrictModel model = response.body();
                    if (model != null && model.getStatusCode() == 200) {
                        if (model.getResult() != null && model.getResult().size() > 0) {
                            districtNameArray.clear();
                            districtIdArray.clear();

                            districtNameArray.add(0, "--Select--");
                            districtIdArray.add(0, "0");
                            for (int i = 0; i < model.getResult().size(); i++) {
                                districtNameArray.add(model.getResult().get(i).getDistrictTitle());
                                districtIdArray.add(model.getResult().get(i).getDistrictid());
                            }
                            districtAdapter = new ArrayAdapter<String>(LocalityActivity.this,
                                    android.R.layout.simple_dropdown_item_1line, districtNameArray);
                            binding.spinnerDistrict.setAdapter(districtAdapter);

                        }
                    } else {
                        assert model != null;
                        showToastMessage(model.getStatusMessage());
                    }
                } else {
                    showToastMessage(response.message());
                }
                hideCustomLoading();

            }

            @Override
            public void onFailure(@NonNull Call<DistrictModel> call, @NonNull Throwable t) {
                if (App.getInstance().isNetworkAvailable(LocalityActivity.this)) {
                    showToastMessage("Something went wrong! please contact your administrator");
                } else {
                    showToastMessage("Please check your internet connection!");
                }

                hideCustomLoading();
            }
        });

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}