package com.village.mygp.view.utilities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.village.mygp.view.ui.activities.LocalityActivity;
import com.village.mygp.view.ui.activities.MainActivity;
import com.village.mygp.view.ui.dialog.SingleButtonAlertDialog;

public class UtilManager  implements Constants{

    public static void showSnackbarMessage(View view, String message) {
        Snackbar.make(view, message, Snackbar.LENGTH_LONG).show();
    }

    public static void showToastMessage(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }


    public static void clearStackLaunchHomeScreen(Activity activity) {
        Intent intent = MainActivity.build(activity);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        activity.startActivity(intent);
        activity.finish();
    }
    public static void clearStackLaunchVillageScreen(Activity activity) {
        Intent intent = LocalityActivity.build(activity);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        activity.startActivity(intent);
        activity.finish();
    }

    public static void showErrorMessage(String msg, Context activity, SingleButtonAlertDialog.ButtonClick clickListener) {

            try {
                Bundle args = new Bundle();
                args.putString("msg", checkStringNull(msg));
                args.putSerializable("listener", clickListener);
                SingleButtonAlertDialog dialog = new SingleButtonAlertDialog(activity, args);
                // dialog.show(fragmentManager, "Dialog");
                dialog.show();
            } catch (Exception ie) {

            }

    }

    public static String checkStringNull(String value) {
        String value1 = "";
        try {
            if (value != null && (!value.equalsIgnoreCase(null)
                    && !value.equalsIgnoreCase("null")
                    && !value.equalsIgnoreCase(""))) {
                value1 = value.trim();
            } else {
                value1 = "";
            }
        } catch (Exception e) {
            value1 = "";
        }
        return value1;
    }
}
