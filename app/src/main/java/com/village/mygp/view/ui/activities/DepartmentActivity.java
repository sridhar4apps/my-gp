package com.village.mygp.view.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DiffUtil;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.village.mygp.R;
import com.village.mygp.databinding.ActivityDepartmentBinding;
import com.village.mygp.model.Card;
import com.village.mygp.view.baseclasses.BaseActivity;
import com.village.mygp.view.ui.adapters.HomeAdapter;

import java.util.ArrayList;
import java.util.List;

public class DepartmentActivity extends BaseActivity implements HomeAdapter.ItemClickListener  {


    public static Intent build(Activity activity) {
        Intent intent = new Intent(activity, DepartmentActivity.class);
        return intent;
    }

    private ActivityDepartmentBinding binding;
    private List<Card> cards;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(DepartmentActivity.this, R.layout.activity_department);

        binding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        HomeAdapter homeAdapter = new HomeAdapter((DiffUtil.ItemCallback<Card>) null, this);
        ArrayList arrayList = new ArrayList();
        this.cards = arrayList;
        this.cards.add(new Card(getString(R.string.electricity), 1,R.drawable.idea));
        this.cards.add(new Card(getString(R.string.parliment), 2,R.drawable.garbage));
        this.cards.add(new Card(getString(R.string.water), 3,R.drawable.water));
        this.cards.add(new Card(getString(R.string.mosquito), 4,R.drawable.mosquito));
        this.cards.add(new Card(getString(R.string.violence), 5,R.drawable.violence));
        this.cards.add(new Card(getString(R.string.fire_accident), 6,R.drawable.accident));
        this.cards.add(new Card(getString(R.string.road_damage), 7,R.drawable.road));
        this.cards.add(new Card(getString(R.string.ambulance), 8,R.drawable.ambulance));
        this.cards.add(new Card(getString(R.string.ration), 9,R.drawable.queue));
        this.cards.add(new Card(getString(R.string.other_needs), 10,R.drawable.question));
        homeAdapter.submitList(this.cards);
        binding.recyclerView.setAdapter(homeAdapter);
    }

    @Override
    public void itemClick(int i) {
        Card card = this.cards.get(i);
        Bundle bundle = new Bundle();
        bundle.putString("department", card.getTitle());
        bundle.putInt("12345", card.getIcon());
        bundle.putInt("category", card.getCategory());
        Intent intent = new Intent(DepartmentActivity.this, ComplaintsActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }
}