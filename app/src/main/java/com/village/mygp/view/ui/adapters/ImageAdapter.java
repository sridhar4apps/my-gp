package com.village.mygp.view.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.village.mygp.R;
import com.village.mygp.model.Card;
import com.village.mygp.model.Welfare;

public class ImageAdapter extends ListAdapter<Welfare, ImageAdapter.ImageViewHolder> {

    public ItemClickListener itemClickListener;

    public interface ItemClickListener {
        void itemClick(int i);
    }

    public ImageAdapter(DiffUtil.ItemCallback<Welfare> itemCallback, ImageAdapter.ItemClickListener itemClickListener2) {
        super(itemCallback);
        this.itemClickListener = itemClickListener2;
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        return new ImageViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.image_card_layout, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder holder, int position) {
        holder.bind((Welfare) getItem(position));
    }


    class ImageViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView title;
        public ImageViewHolder(@NonNull View itemView) {
            super(itemView);
            this.title = (TextView) itemView.findViewById(R.id.title);
            this.image = (ImageView) itemView.findViewById(R.id.image);
        }

        public void bind(Welfare item) {
            this.title.setText(item.getTitle());
            this.image.setImageResource(item.getIcon());
        }
    }
}
