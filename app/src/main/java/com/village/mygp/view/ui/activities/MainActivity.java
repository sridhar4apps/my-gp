package com.village.mygp.view.ui.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DiffUtil;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.denzcoskun.imageslider.constants.ScaleTypes;
import com.denzcoskun.imageslider.models.SlideModel;
import com.google.android.material.navigation.NavigationView;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.OnSuccessListener;
import com.village.mygp.BuildConfig;
import com.village.mygp.R;
import com.village.mygp.databinding.ActivityMainBinding;
import com.village.mygp.model.BannerModel;
import com.village.mygp.model.Card;
import com.village.mygp.model.TokenModel;
import com.village.mygp.view.api.RetrofitApis;
import com.village.mygp.view.baseclasses.BaseActivity;
import com.village.mygp.view.ui.adapters.HomeAdapter;
import com.village.mygp.view.utilities.FlavorName;
import com.village.mygp.view.utilities.SharedPreferenceDataUtil;
import com.village.mygp.view.utilities.UtilManager;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener, HomeAdapter.ItemClickListener {

    public static Intent build(Activity activity) {
        Intent intent = new Intent(activity, MainActivity.class);
        return intent;
    }

    private List<Card> cards;
    private ActivityMainBinding binding;
    private SharedPreferenceDataUtil sharedPreferenceDataUtil;
    private List<SlideModel> imageList = new ArrayList<>();


    private AppUpdateManager mAppUpdateManager;

    private static final int RC_APP_UPDATE = 100;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(MainActivity.this, R.layout.activity_main);
        sharedPreferenceDataUtil = new SharedPreferenceDataUtil(this);
        //setContentView(R.layout.activity_main);

        HomeAdapter homeAdapter = new HomeAdapter((DiffUtil.ItemCallback<Card>) null, this);
        ArrayList arrayList = new ArrayList();
        if (FlavorName.SARPANCH.name().equalsIgnoreCase(BuildConfig.FLAVOR) || FlavorName.TRSSARPANCH.name().equalsIgnoreCase(BuildConfig.FLAVOR)) {
            arrayList.add(new Card(getString(R.string.messages), 1, R.drawable.message));
            arrayList.add(new Card(getString(R.string.complaint_box), 1, R.drawable.complaint));
            arrayList.add(new Card(getString(R.string.covid), 4, R.drawable.gallery));
            arrayList.add(new Card(getString(R.string.welfare_programs), 2, R.drawable.welfare_programs));
            arrayList.add(new Card(getString(R.string.departments), 5, R.drawable.parliament));
        } else {
            arrayList.add(new Card(getString(R.string.complaint_box), 1, R.drawable.complaint));
            arrayList.add(new Card(getString(R.string.messages), 1, R.drawable.message));
            arrayList.add(new Card(getString(R.string.resolved), 3, R.drawable.resolved));
            arrayList.add(new Card(getString(R.string.welfare_programs), 2, R.drawable.welfare_programs));
            arrayList.add(new Card(getString(R.string.covid), 4, R.drawable.gallery));
            arrayList.add(new Card(getString(R.string.departments), 5, R.drawable.parliament));
        }

        this.cards = arrayList;
        homeAdapter.submitList(arrayList);
        binding.recyclerView.setAdapter(homeAdapter);
        binding.menuIcon.setOnClickListener(view -> binding.drawerLayout.openDrawer(GravityCompat.START));

        if(isNetworkAvailable())
        {
            mAppUpdateManager = AppUpdateManagerFactory.create(this);
            mAppUpdateManager.getAppUpdateInfo().addOnSuccessListener(new OnSuccessListener<AppUpdateInfo>() {
                @Override
                public void onSuccess(AppUpdateInfo result) {
                    if (result.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                            && result.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)) {
                        try {
                            mAppUpdateManager.startUpdateFlowForResult(result, AppUpdateType.IMMEDIATE, MainActivity.this
                                    , RC_APP_UPDATE);

                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

            getBannerData(sharedPreferenceDataUtil.getVillageId());
        }


        /*if (FlavorName.SARPANCH.name().equalsIgnoreCase(BuildConfig.FLAVOR) || FlavorName.PEOPLE.name().equalsIgnoreCase(BuildConfig.FLAVOR)) {
            imageList.add(new SlideModel(R.drawable.banner2, ScaleTypes.FIT));
            imageList.add(new SlideModel(R.drawable.banner1, ScaleTypes.FIT));
            imageList.add(new SlideModel(R.drawable.banner3, ScaleTypes.FIT));
            imageList.add(new SlideModel(R.drawable.banner2, ScaleTypes.FIT));
            imageList.add(new SlideModel(R.drawable.banner1, ScaleTypes.FIT));
            binding.imageSlider.setImageList(imageList, ScaleTypes.FIT);
        }
        if (FlavorName.TRSSARPANCH.name().equalsIgnoreCase(BuildConfig.FLAVOR) || FlavorName.TRSUSER.name().equalsIgnoreCase(BuildConfig.FLAVOR)) {
            imageList.add(new SlideModel(R.drawable.banner11, ScaleTypes.FIT));
            imageList.add(new SlideModel(R.drawable.banner22, ScaleTypes.FIT));
            imageList.add(new SlideModel(R.drawable.banner11, ScaleTypes.FIT));
            imageList.add(new SlideModel(R.drawable.banner33, ScaleTypes.FIT));
            imageList.add(new SlideModel(R.drawable.banner11, ScaleTypes.FIT));
            binding.imageSlider.setImageList(imageList, ScaleTypes.FIT);
        }*/
        hideItem();

        binding.navigationViewLayout.setItemIconTintList(null);
        binding.navigationViewLayout.setNavigationItemSelectedListener(this);


    }

    private void hideItem() {
        if (FlavorName.SARPANCH.name().equalsIgnoreCase(BuildConfig.FLAVOR))
        {
            Menu nav_Menu = binding.navigationViewLayout.getMenu();
            nav_Menu.findItem(R.id.menu_village).setVisible(false);

        }else{

        }
    }

    private void getBannerData(String villageId) {
        showCustomLoading("");
        RetrofitApis retrofitApis = RetrofitApis.Factory.create();
        Call<BannerModel> bannerModelCall = retrofitApis.getBannerData(villageId);
        bannerModelCall.enqueue(new Callback<BannerModel>() {
            @Override
            public void onResponse(Call<BannerModel> call, Response<BannerModel> response) {
                if(response.isSuccessful())
                {
                    BannerModel model = response.body();
                    if (model.getData() != null && model.getData().size() > 0) {
                        binding.noBannerImg.setVisibility(View.GONE);
                        binding.imageSlider.setVisibility(View.VISIBLE);
                        for(int i=0;i<model.getData().size();i++){
                            imageList.add(new SlideModel(model.getData().get(i).getUrl(), ScaleTypes.FIT));
                        }
                        binding.imageSlider.setImageList(imageList, ScaleTypes.FIT);
                    }else{

                        binding.noBannerImg.setVisibility(View.VISIBLE);
                        binding.imageSlider.setVisibility(View.GONE);
                    }

                }else{
                    binding.noBannerImg.setVisibility(View.VISIBLE);
                    binding.imageSlider.setVisibility(View.GONE);
                }

                hideCustomLoading();
            }

            @Override
            public void onFailure(Call<BannerModel> call, Throwable t) {
                binding.noBannerImg.setVisibility(View.VISIBLE);
                binding.imageSlider.setVisibility(View.GONE);
                hideCustomLoading();
            }
        });
    }

    @Override
    public void itemClick(int i) {
        binding.drawerLayout.closeDrawer(GravityCompat.START);
        Card card = cards.get(i);
        Bundle bundle = new Bundle();
        bundle.putString("types", card.getTitle());
        bundle.putInt("value", card.getCategory());
        if (FlavorName.SARPANCH.name().equalsIgnoreCase(BuildConfig.FLAVOR) || FlavorName.TRSSARPANCH.name().equalsIgnoreCase(BuildConfig.FLAVOR)) {
            if (i == 0) {
                Intent intent = new Intent(MainActivity.this, MessageActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);

            }
            if (i == 1) {

                Intent intent = new Intent(MainActivity.this, ComplaintListActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
            if (i == 2) {

                showToastMessage("Under Construction");
            }
            if (i == 3) {
                Intent intent = new Intent(MainActivity.this, MessageActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }

            if (i == 4) {
                Intent intent = new Intent(MainActivity.this, HelpLineActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
                //showToastMessage("Under Construction");
            }

        } else {
            if (i == 0) {

                showCustomLoading("");
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        hideCustomLoading();

                        Intent build = DepartmentActivity.build(MainActivity.this);
                        startActivity(build);


                    }
                }, 2000);

            }
            if (i == 1) {
                Intent intent = new Intent(MainActivity.this, GalleryActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
            if (i == 2) {
                Intent intent = new Intent(MainActivity.this, ComplaintListActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
            if (i == 3) {
                Intent intent = new Intent(MainActivity.this, GalleryActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
                //showToastMessage("Under Construction");
            }
            if (i == 4) {
                showToastMessage("Under Construction");
            }
            if (i == 5) {
                Intent intent = new Intent(MainActivity.this, HelpLineActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        }


    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        binding.drawerLayout.closeDrawer(GravityCompat.START);
        if (id == R.id.menu_home) {
            showCustomLoading("");
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    hideCustomLoading();
                    UtilManager.clearStackLaunchHomeScreen(MainActivity.this);
                }
            }, 1000);

        }
        if (FlavorName.SARPANCH.name().equalsIgnoreCase(BuildConfig.FLAVOR))
        {


        }else{
            if (id == R.id.menu_village) {
                showCustomLoading("");
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        hideCustomLoading();

                        sharedPreferenceDataUtil.clearAllPreferences(MainActivity.this);
                        UtilManager.clearStackLaunchVillageScreen(MainActivity.this);
                    }
                }, 1000);
            }
        }


        if (id == R.id.menu_lang) {
            Intent build = LanguageActivity.build(MainActivity.this);
            startActivity(build);
        }
        if (id == R.id.menu_rate) {

        }
        if (id == R.id.menu_privacy) {

        }
        if (id == R.id.menu_about_us) {

        }
        return false;
    }


    @Override
    public void onBackPressed() {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(
                MainActivity.this);
        mBuilder.setIcon(R.mipmap.ic_launcher_round);
        mBuilder.setTitle("Rate US");
        mBuilder.setMessage("Thank You For Using Our Application Please Give Us Your Rating and Review.");
        mBuilder.setPositiveButton("RATE US", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (isNetworkAvailable()) {
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW,
                                Uri.parse("market://details?id=" + getPackageName())));
                    } catch (Exception e) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + getPackageName())));

                    }

                } else {
                    showToastMessage("Please connect Internet.......");
                }
                dialog.dismiss();

            }
        });
        mBuilder.setNegativeButton("EXIT",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        moveTaskToBack(true);
                        android.os.Process.killProcess(android.os.Process.myPid());
                        System.exit(1);
                        dialog.dismiss();


                    }
                });
        mBuilder.create();
        mBuilder.show();


    }

    @Override
    protected void onResume() {
        super.onResume();
        if(isNetworkAvailable())
        {
            mAppUpdateManager.getAppUpdateInfo().addOnSuccessListener(new OnSuccessListener<AppUpdateInfo>() {
                @Override
                public void onSuccess(AppUpdateInfo result) {
                    if (result.updateAvailability() == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS) {
                        try {
                            mAppUpdateManager.startUpdateFlowForResult(result, AppUpdateType.IMMEDIATE, MainActivity.this
                                    , RC_APP_UPDATE);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
    }
}