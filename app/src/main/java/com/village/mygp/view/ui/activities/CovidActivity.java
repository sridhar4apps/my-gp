package com.village.mygp.view.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DiffUtil;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.village.mygp.R;
import com.village.mygp.databinding.ActivityCovidBinding;
import com.village.mygp.model.Card;
import com.village.mygp.view.baseclasses.BaseActivity;
import com.village.mygp.view.ui.adapters.HomeAdapter;

import java.util.ArrayList;
import java.util.List;

public class CovidActivity extends BaseActivity implements HomeAdapter.ItemClickListener  {

    private ActivityCovidBinding binding;
    private List<Card> cards;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(CovidActivity.this,R.layout.activity_covid);
        binding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        Bundle bundle = getIntent().getExtras();
        binding.complaintName.setText(bundle.getString("types"));
       /* HomeAdapter homeAdapter = new HomeAdapter((DiffUtil.ItemCallback<Card>) null, this);
        ArrayList arrayList = new ArrayList();
        arrayList.add(new Card(getString(R.string.corona_report), R.drawable.report));
        arrayList.add(new Card(getString(R.string.corona_help), R.drawable.corona_help));
        this.cards = arrayList;
        homeAdapter.submitList(this.cards);
        binding.recyclerView.setAdapter(homeAdapter);*/

    }

    @Override
    public void itemClick(int i) {
        Card card = this.cards.get(i);
        Bundle bundle = new Bundle();
        bundle.putString("department", card.getTitle());
        bundle.putInt("12345", card.getIcon());
        Intent intent = new Intent(CovidActivity.this, ComplaintsActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }
}