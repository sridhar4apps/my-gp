package com.village.mygp.view.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.AsyncDifferConfig;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.village.mygp.BuildConfig;
import com.village.mygp.R;
import com.village.mygp.model.Card;
import com.village.mygp.model.ComplaintListModel;
import com.village.mygp.view.utilities.FlavorName;

public class ComplaintListAdapter extends ListAdapter<ComplaintListModel.Result, ComplaintListAdapter.ComplaintViewHolder> {
    public ItemClickListener itemClickListener;


    public interface ItemClickListener {
        void itemClick(int i);
    }

    public ComplaintListAdapter(DiffUtil.ItemCallback<ComplaintListModel.Result> itemCallback, ItemClickListener itemClickListener2) {
        super(itemCallback);
        this.itemClickListener = itemClickListener2;
    }

    @NonNull
    @Override
    public ComplaintViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ComplaintViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_complaint, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ComplaintViewHolder holder, int position) {
        holder.bind((ComplaintListModel.Result) getItem(position));
    }

    public class ComplaintViewHolder extends RecyclerView.ViewHolder {
        TextView user_name;
        TextView user_mobile;
        TextView user_ward_no;
        TextView user_description;
        TextView timestamp;
        TextView complaintStatus,response;
        AppCompatButton btnSubmit;
        LinearLayout response_layout;

        public ComplaintViewHolder(@NonNull View itemView) {
            super(itemView);
            user_name = itemView.findViewById(R.id.user_name);
            user_mobile = itemView.findViewById(R.id.user_mobile);
            user_ward_no = itemView.findViewById(R.id.user_ward_no);
            user_description = itemView.findViewById(R.id.user_description);
            timestamp = itemView.findViewById(R.id.timestamp);
            complaintStatus = itemView.findViewById(R.id.complaintStatus);
            btnSubmit = itemView.findViewById(R.id.btnSubmit);
            response_layout = itemView.findViewById(R.id.response_layout);
            response = itemView.findViewById(R.id.response);
            btnSubmit.setOnClickListener(new View.OnClickListener() {
                public final void onClick(View view) {
                    ComplaintListAdapter.ComplaintViewHolder.this.getNew(view);
                }
            });
        }

        private void getNew(View view) {
            ComplaintListAdapter.this.itemClickListener.itemClick(getAdapterPosition());
        }



        public void bind(ComplaintListModel.Result item) {
            user_name.setText(item.getUserName());
            user_mobile.setText(item.getUserMobile());
            user_ward_no.setText(item.getUserWardNo());
            user_description.setText(item.getUserDescription());
            timestamp.setText(item.getCreatedAt());
            response.setText(item.getComplaintResponse());

            if (item.getCurrentStatus().equals("1")) {
                complaintStatus.setText("closed");
                response_layout.setVisibility(View.VISIBLE);

            } else {
                complaintStatus.setText("pending");
                response_layout.setVisibility(View.GONE);
            }

            if(FlavorName.SARPANCH.name().equalsIgnoreCase(BuildConfig.FLAVOR) ){
                if (item.getCurrentStatus().equals("1")) {
                    btnSubmit.setVisibility(View.GONE);
                    response_layout.setVisibility(View.VISIBLE);
                } else {
                    btnSubmit.setVisibility(View.VISIBLE);
                    response_layout.setVisibility(View.GONE);
                }
            }else{
                btnSubmit.setVisibility(View.GONE);
                response_layout.setVisibility(View.VISIBLE);
            }

        }
    }
}
