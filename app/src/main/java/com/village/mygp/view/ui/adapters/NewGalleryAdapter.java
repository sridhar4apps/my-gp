package com.village.mygp.view.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.village.mygp.R;
import com.village.mygp.model.GalleryModel;
import com.village.mygp.view.ui.activities.MessageActivity;

import java.util.ArrayList;

public class NewGalleryAdapter extends RecyclerView.Adapter<NewGalleryAdapter.ViewHolder> {

    private Context context;
    private LayoutInflater inflater;
    private ArrayList<GalleryModel.Result> arrayList;

    public NewGalleryAdapter(Context messageActivity, ArrayList<GalleryModel.Result> list) {
        context = messageActivity;
        arrayList = list;
        this.inflater = LayoutInflater.from(messageActivity);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.image_card_layout, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.title.setText(arrayList.get(position).getDescription());
        Glide.with(context).load(arrayList.get(position).getFilePath()).into(holder.image);

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView title;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.title = itemView.findViewById(R.id.title);
            this.image = itemView.findViewById(R.id.image);
        }
    }
}
