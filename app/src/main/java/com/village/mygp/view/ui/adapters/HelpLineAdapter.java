package com.village.mygp.view.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.village.mygp.BuildConfig;
import com.village.mygp.R;
import com.village.mygp.model.HelpLineModel;
import com.village.mygp.view.ui.activities.HelpLineActivity;
import com.village.mygp.view.utilities.FlavorName;

import java.util.ArrayList;

public class HelpLineAdapter extends RecyclerView.Adapter<HelpLineAdapter.ViewHolder> {
    private Context context;
    private LayoutInflater inflater;
    private ArrayList<HelpLineModel.Datum> mArrayList;
    private HelpLineActivity activity;

    public HelpLineAdapter(Context helpLineActivity, ArrayList<HelpLineModel.Datum> arrayList, HelpLineActivity mContext) {
        context = helpLineActivity;
        activity = mContext;
        mArrayList = arrayList;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.help_line_adapter, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.designationTitle.setText("Designation: "+mArrayList.get(position).getDesignation());
        holder.mobileNumber.setText("Mobile: "+mArrayList.get(position).getNumber()+"  ");
        holder.mobileNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent helpIntent = new Intent(Intent.ACTION_DIAL);
                helpIntent.setData(Uri.parse("tel:" + (mArrayList.get(position).getNumber().replaceAll("[\\s\\-()]", ""))));
                context.startActivity(helpIntent);
            }
        });

        holder.deleteClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.deleteItem(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView designationTitle, mobileNumber;
        private AppCompatImageView deleteClick;
        private CardView layoutClick;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            designationTitle = itemView.findViewById(R.id.designationTitle);
            mobileNumber = itemView.findViewById(R.id.mobileNumber);
            deleteClick = itemView.findViewById(R.id.deleteClick);
            layoutClick = itemView.findViewById(R.id.layoutClick);

            if (FlavorName.SARPANCH.name().equalsIgnoreCase(BuildConfig.FLAVOR)) {
                deleteClick.setVisibility(View.VISIBLE);
            }else{
                deleteClick.setVisibility(View.GONE);
            }
        }
    }
}
