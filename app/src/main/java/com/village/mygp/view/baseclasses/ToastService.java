package com.village.mygp.view.baseclasses;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.core.content.res.ResourcesCompat;

import com.pd.chocobar.ChocoBar;
import com.village.mygp.R;

public class ToastService extends ContextWrapper {
    private Context context;

    public ToastService(Context context2) {
        super(context2);
        this.context = context2;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void info(String str) {
        ChocoBar.builder().setActivity((Activity) this.context).setText((CharSequence) str)
                .setActionText((CharSequence) "Okay").setDuration(ChocoBar.LENGTH_INDEFINITE)
                .setTextTypeface(ResourcesCompat.getFont(this, R.font.open_sans_bold))
                .setActionTextColor(getColor(R.color.colorPrimary))
                .setIcon((int) R.drawable.ic_signal_cellular_off_black_24dp).build().show();
    }

    public void danger(String str) {
        ChocoBar.builder().setActivity((Activity) this.context).setText(str)
                .setTextTypeface(ResourcesCompat.getFont(this, R.font.open_sans_bold)).red().show();
    }

    public void success(String str) {
        ChocoBar.builder().setActivity((Activity) this.context).setText((CharSequence) str)
                .setTextTypeface(ResourcesCompat.getFont(this, R.font.open_sans_bold)).green().show();
    }

    public void error() {
        danger("Something went wrong!");
    }
}
