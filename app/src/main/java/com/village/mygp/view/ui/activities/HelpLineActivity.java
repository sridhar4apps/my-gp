package com.village.mygp.view.ui.activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.village.mygp.BuildConfig;
import com.village.mygp.R;
import com.village.mygp.application.App;
import com.village.mygp.databinding.ActivityHelpLineBinding;
import com.village.mygp.model.HelpLineModel;
import com.village.mygp.model.HelpLineSubModel;
import com.village.mygp.model.HelplineDeletModel;
import com.village.mygp.view.api.RetrofitApis;
import com.village.mygp.view.baseclasses.BaseActivity;
import com.village.mygp.view.baseclasses.ToastService;
import com.village.mygp.view.ui.adapters.HelpLineAdapter;
import com.village.mygp.view.utilities.FlavorName;
import com.village.mygp.view.utilities.SharedPreferenceDataUtil;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HelpLineActivity extends BaseActivity {

    private String catId;
    private ActivityHelpLineBinding binding;
    private SharedPreferenceDataUtil sharedPreferenceDataUtil;
    private HelpLineAdapter helpLineAdapter;
    private ArrayList<HelpLineModel.Datum> arrayList = new ArrayList<>();
    private ToastService toastService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(HelpLineActivity.this, R.layout.activity_help_line);
        //setContentView(R.layout.activity_help_line);
        sharedPreferenceDataUtil = new SharedPreferenceDataUtil(this);
        toastService = new ToastService(this);

        Bundle bundle = getIntent().getExtras();
        binding.complainName.setText(bundle.getString("types"));
        catId = String.valueOf(bundle.getInt("value"));


        helpLineAdapter = new HelpLineAdapter(HelpLineActivity.this, arrayList, this);
        binding.recyclerView.setAdapter(helpLineAdapter);

        binding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        if (FlavorName.SARPANCH.name().equalsIgnoreCase(BuildConfig.FLAVOR)) {
            binding.linCenter.setVisibility(View.VISIBLE);
        } else {
            binding.linCenter.setVisibility(View.GONE);

        }
        if (isNetworkAvailable()) {

            getHelpLineData(sharedPreferenceDataUtil.getVillageId());
        }

        binding.btnSubmit.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                if (isNetworkAvailable()) {
                    if (validation()) {

                        if (isNetworkAvailable()) {
                            submitHelpLine();

                        } else {
                            toastService.info("No Internet!");
                        }
                    }
                } else {
                    toastService.info("No Internet!");
                }
            }
        });

    }

    private void submitHelpLine() {
        showCustomLoading("");
        RetrofitApis retrofitApis = RetrofitApis.Factory.create();
        Call<HelpLineSubModel> lineSubModelCall = retrofitApis.helpLineSubmit(sharedPreferenceDataUtil.getVillageId(),binding.etName.getText().toString().trim(),
                binding.etMobile.getText().toString().trim());
        lineSubModelCall.enqueue(new Callback<HelpLineSubModel>() {
            @Override
            public void onResponse(Call<HelpLineSubModel> call, Response<HelpLineSubModel> response) {
                if(response.isSuccessful())
                {
                    HelpLineSubModel helpLineSubModel = response.body();
                    if(helpLineSubModel.getStatusCode() != null && helpLineSubModel.getStatusCode() == 200)
                    {
                        binding.etMobile.setText("");
                        binding.etName.setText("");
                        binding.etName.requestFocus();
                        showToastMessage(helpLineSubModel.getStatusMessage());
                        getHelpLineData(sharedPreferenceDataUtil.getVillageId());
                    }else{
                        showToastMessage(helpLineSubModel.getStatusMessage());
                    }

                }else{

                    showToastMessage(response.message());
                }
                hideCustomLoading();
            }

            @Override
            public void onFailure(Call<HelpLineSubModel> call, Throwable t) {
                if (App.getInstance().isNetworkAvailable(HelpLineActivity.this)) {
                    showToastMessage("Something went wrong! please contact your administrator");
                } else {
                    showToastMessage("Please check your internet connection!");
                }
                hideCustomLoading();
            }
        });
    }

    private boolean validation() {
        if (TextUtils.isEmpty(binding.etName.getText().toString())) {
            binding.etName.requestFocus();
            showToastMessage("Please Enter Designation");
            return false;
        }
        if (!validatePhNo(getApplicationContext(), binding.etMobile)) {
            binding.etMobile.requestFocus();
            return false;
        }

        return true;
    }
    private void getHelpLineData(String villageId) {
        showCustomLoading("");
        RetrofitApis retrofitApis = RetrofitApis.Factory.create();
        Call<HelpLineModel> helpLineModelCall = retrofitApis.getHelpLineData(villageId);
        helpLineModelCall.enqueue(new Callback<HelpLineModel>() {
            @Override
            public void onResponse(Call<HelpLineModel> call, Response<HelpLineModel> response) {
                if (response.isSuccessful()) {
                    HelpLineModel model = response.body();
                    if (model.getStatusCode() != null && model.getStatusCode() == 200) {
                        if (model.getData() != null && model.getData().size() > 0) {

                            arrayList.clear();
                            arrayList.addAll(model.getData());
                            helpLineAdapter.notifyDataSetChanged();

                        }
                    } else {
                        showToastMessage(model.getStatusMessage());
                    }
                } else {
                    showToastMessage(response.message());
                }

                hideCustomLoading();
            }

            @Override
            public void onFailure(Call<HelpLineModel> call, Throwable t) {

                if (App.getInstance().isNetworkAvailable(HelpLineActivity.this)) {
                    showToastMessage("Something went wrong! please contact your administrator");
                } else {
                    showToastMessage("Please check your internet connection!");
                }
                hideCustomLoading();
            }
        });
    }

    public void deleteItem(int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(HelpLineActivity.this);
        builder.setTitle(""+arrayList.get(position).getDesignation());
        builder.setMessage("Do you delete this number?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                deleteHelpLineNumber(sharedPreferenceDataUtil.getVillageId(),arrayList.get(position).getHelplineId());

            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }

    private void deleteHelpLineNumber(String villageId, String helplineId) {
        showCustomLoading("");
        RetrofitApis retrofitApis = RetrofitApis.Factory.create();
        Call<HelplineDeletModel> modelCall = retrofitApis.helpLineDelete(villageId,helplineId);
        modelCall.enqueue(new Callback<HelplineDeletModel>() {
            @Override
            public void onResponse(Call<HelplineDeletModel> call, Response<HelplineDeletModel> response) {
                if(response.isSuccessful())
                {
                    HelplineDeletModel helpLineSubModel = response.body();
                    if(helpLineSubModel.getStatusCode() != null && helpLineSubModel.getStatusCode() == 200)
                    {
                        showToastMessage(helpLineSubModel.getStatusMessage());
                        getHelpLineData(sharedPreferenceDataUtil.getVillageId());
                    }else{
                        showToastMessage(helpLineSubModel.getStatusMessage());
                    }

                }else{

                    showToastMessage(response.message());
                }
                //hideCustomLoading();
            }

            @Override
            public void onFailure(Call<HelplineDeletModel> call, Throwable t) {
                if (App.getInstance().isNetworkAvailable(HelpLineActivity.this)) {
                    showToastMessage("Something went wrong! please contact your administrator");
                } else {
                    showToastMessage("Please check your internet connection!");
                }
                hideCustomLoading();
            }
        });
    }
}