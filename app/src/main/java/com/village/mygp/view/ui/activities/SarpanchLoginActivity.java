package com.village.mygp.view.ui.activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.village.mygp.BuildConfig;
import com.village.mygp.R;
import com.village.mygp.databinding.ActivitySarpanchLoginBinding;
import com.village.mygp.model.SarpanchModel;
import com.village.mygp.model.TokenModel;
import com.village.mygp.view.api.RetrofitApis;
import com.village.mygp.view.baseclasses.BaseActivity;
import com.village.mygp.view.baseclasses.ToastService;
import com.village.mygp.view.utilities.FlavorName;
import com.village.mygp.view.utilities.SharedPreferenceDataUtil;
import com.village.mygp.view.utilities.UtilManager;
import com.village.mygp.viewmodel.LoginViewModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SarpanchLoginActivity extends BaseActivity {
    public static Intent build(Activity activity) {
        Intent intent = new Intent(activity, SarpanchLoginActivity.class);
        return intent;
    }

    private ActivitySarpanchLoginBinding binding;
    private LoginViewModel loginViewModel;
    private ToastService toastService;
    private SharedPreferenceDataUtil sharedPreferenceDataUtil;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(SarpanchLoginActivity.this,R.layout.activity_sarpanch_login);
        this.toastService = new ToastService(this);
        sharedPreferenceDataUtil = new SharedPreferenceDataUtil(this);

        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        binding.btnSubmit.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                if(isNetworkAvailable())
                {
                    if (validation()) {

                        getSarpanchData(binding.etMobile.getText().toString().trim(),binding.etEmail.getText().toString().trim());
                    }
                }else{
                    toastService.info("No Internet!");
                }

            }
        });

        loginViewModel.getLoginData().observe(SarpanchLoginActivity.this, new Observer<SarpanchModel>() {
            @Override
            public void onChanged(SarpanchModel sarpanchModel) {
                if (sarpanchModel.getSuccess() != null && sarpanchModel.getSuccess().equalsIgnoreCase("true")) {
                    toastService.success(sarpanchModel.getMessage());
                    sharedPreferenceDataUtil.setLocationSelect(true);
                    sharedPreferenceDataUtil.settUserId(sarpanchModel.getUserid());
                    sharedPreferenceDataUtil.setVillageId(sarpanchModel.getVillageId());

                    //sharedPreferenceDataUtil.setAppTheme("TDP");
                    if (FlavorName.SARPANCH.name().equalsIgnoreCase(BuildConfig.FLAVOR) || FlavorName.TRSSARPANCH.name().equalsIgnoreCase(BuildConfig.FLAVOR)) {
                        saveToken(getPreferToken(SharedPreferenceDataUtil.refreshedToken),sharedPreferenceDataUtil.getVillageId(),"1",sharedPreferenceDataUtil.getUserId());
                    }else{
                        saveToken(getPreferToken(SharedPreferenceDataUtil.refreshedToken),sharedPreferenceDataUtil.getVillageId(),"2","");

                    }


                }else{
                   // toastService.danger(sarpanchModel.getSuccess());
                }
                hideCustomLoading();
            }
        });

        loginViewModel.getMessageToShow().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                toastService.danger(s);
                hideCustomLoading();
            }
        });

    }

    private void getSarpanchData(String mobile, String email) {
        showCustomLoading("");
        loginViewModel.getSarpanch(SarpanchLoginActivity.this,mobile,email);
    }

    private boolean validation() {
        if (!validatePhNo(getApplicationContext(),binding.etMobile)) {
            binding.etEmail.requestFocus();
            return false;
        } if (TextUtils.isEmpty(binding.etEmail.getText().toString())) {
            binding.etEmail.requestFocus();
            UtilManager.showToastMessage(SarpanchLoginActivity.this, "Please enter a valid password");
            return false;
        }

        return true;
    }

    private boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();

    }


    private void saveToken(String notifyToken, String villageId, String userType, String userId) {
        showCustomLoading("");
        RetrofitApis retrofitApis = RetrofitApis.Factory.create();
        Call<TokenModel> tokenModelCall = retrofitApis.tokenSave(notifyToken,villageId,userType,userId);
        tokenModelCall.enqueue(new Callback<TokenModel>() {
            @Override
            public void onResponse(Call<TokenModel> call, Response<TokenModel> response) {
                if(response.isSuccessful())
                {
                    TokenModel model = response.body();
                    if(model.getSuccess().equalsIgnoreCase("true"))
                    {
                        Intent build = MainActivity.build(SarpanchLoginActivity.this);
                        startActivity(build);
                        finish();
                    }
                    showToastMessage(model.getMessage());
                }else{
                    showToastMessage(response.message());
                }
                hideCustomLoading();
            }

            @Override
            public void onFailure(Call<TokenModel> call, Throwable t) {
                hideCustomLoading();
            }
        });
    }
}