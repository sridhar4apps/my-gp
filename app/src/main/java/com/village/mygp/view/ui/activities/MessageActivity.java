package com.village.mygp.view.ui.activities;

import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.loader.content.CursorLoader;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.village.mygp.BuildConfig;
import com.village.mygp.R;
import com.village.mygp.databinding.ActivityMessageBinding;
import com.village.mygp.model.GalleryModel;
import com.village.mygp.model.MessageModel;
import com.village.mygp.model.Welfare;
import com.village.mygp.view.baseclasses.BaseActivity;
import com.village.mygp.view.baseclasses.ToastService;
import com.village.mygp.view.ui.adapters.GalleryAdapter;
import com.village.mygp.view.ui.adapters.NewGalleryAdapter;
import com.village.mygp.view.ui.dialog.SingleButtonAlertDialog;
import com.village.mygp.view.utilities.SharedPreferenceDataUtil;
import com.village.mygp.view.utilities.UtilManager;
import com.village.mygp.viewmodel.GalleryViewModel;
import com.village.mygp.viewmodel.MessageViewModel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Logger;

public class MessageActivity extends BaseActivity implements GalleryAdapter.ItemClickListener {

    private ActivityMessageBinding binding;
    public List<Welfare> welfareList;


    private Uri fileUri;
    private String mediaPath;
    private String mImageFileLocation = "";
    public static final String IMAGE_DIRECTORY_NAME = "Android File Upload";
    private static final int CAMERA_PIC_REQUEST = 1111;
    private static final int GALLERY_PIC_REQUEST = 1112;
    private String file_path;
    private SharedPreferenceDataUtil sharedPreferenceDataUtil;
    private MessageViewModel viewModel;
    private ToastService toastService;
    private GalleryViewModel galleryViewModel;
    private NewGalleryAdapter adapter;
    private String catId;
    private boolean pic_done = false;
    private ArrayList<GalleryModel.Result> arrayList = new ArrayList<>();
    ;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(MessageActivity.this, R.layout.activity_message);
        sharedPreferenceDataUtil = new SharedPreferenceDataUtil(this);
        toastService = new ToastService(this);
        viewModel = new ViewModelProvider(this).get(MessageViewModel.class);
        sharedPreferenceDataUtil.setImagePath("");
        galleryViewModel = new ViewModelProvider(this).get(GalleryViewModel.class);
        Bundle bundle = getIntent().getExtras();
        binding.complainName.setText(bundle.getString("types"));
        catId = String.valueOf(bundle.getInt("value"));

        adapter = new NewGalleryAdapter(MessageActivity.this, arrayList);
        binding.recyclerView.setAdapter(adapter);
        if (isNetworkAvailable()) {
            getGalleryData(sharedPreferenceDataUtil.getVillageId(),catId);
        } else {
            toastService.info("No Internet!");
        }


        binding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        binding.fileButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });


        binding.btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isNetworkAvailable()) {
                    if (validation()) {

                        getMessageData();
                    }
                }
            }
        });


        galleryViewModel.getData().observe(MessageActivity.this, new Observer<GalleryModel>() {
            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void onChanged(GalleryModel qaModel) {
                if (qaModel.getStatusCode() != null && qaModel.getStatusCode() == 200) {
                    if (qaModel.getResult() != null && qaModel.getResult().size() > 0) {
                        binding.recyclerView.setVisibility(View.VISIBLE);
                        binding.empty.setVisibility(View.GONE);
                        arrayList.clear();
                        arrayList.addAll(qaModel.getResult());
                        adapter.notifyDataSetChanged();
                    }
                } else {
                    binding.recyclerView.setVisibility(View.GONE);
                    binding.empty.setVisibility(View.VISIBLE);
                }
                hideCustomLoading();
            }
        });

        galleryViewModel.getMessageToShow().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                toastService.danger(s);
                hideCustomLoading();

                binding.recyclerView.setVisibility(View.GONE);
                binding.empty.setVisibility(View.VISIBLE);
            }
        });


        viewModel.getMessageData().observe(MessageActivity.this, new Observer<MessageModel>() {
            @Override
            public void onChanged(MessageModel messageModel) {
                if (messageModel.getStatusCode() != null && messageModel.getStatusCode() == 200) {
                    UtilManager.showErrorMessage(messageModel.getStatusMessage(), MessageActivity.this, new SingleButtonAlertDialog.ButtonClick() {
                        @Override
                        public void onClick(View v) {

                            pic_done = true;
                            getViewModelStore().clear();
                            getGalleryData(sharedPreferenceDataUtil.getVillageId(),catId);
                            //finish();
                        }
                    });
                } else {

                }
                hideCustomLoading();
            }
        });

        viewModel.getMessageToShow().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                toastService.danger(s);
                hideCustomLoading();
            }
        });
    }


    private boolean validation() {
      /*  if (!validatePhoto()) {
            return false;
        }*/
        if (TextUtils.isEmpty(binding.etMsg.getText().toString())) {
            binding.etMsg.requestFocus();
            showToastMessage("Please Enter Message");
            return false;
        }
        return true;
    }

    private boolean validatePhoto() {

        if (!sharedPreferenceDataUtil.getImagePath().isEmpty()) {
            return true;
        }
        showToastMessage("Please Select Image");
        return false;
    }

    @Override
    public void itemClick(int i) {

    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    private void selectImage() {
        if (Build.VERSION.SDK_INT > 21) { //use this if Lollipop_Mr1 (API 22) or above

            try {
                PackageManager pm = this.getPackageManager();
                int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, this.getPackageName());
                int hasPerm2 = pm.checkPermission(Manifest.permission.READ_EXTERNAL_STORAGE, this.getPackageName());
                int hasPerm3 = pm.checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, this.getPackageName());

                if (hasPerm == PackageManager.PERMISSION_GRANTED && hasPerm2 == PackageManager.PERMISSION_GRANTED
                        && hasPerm3 == PackageManager.PERMISSION_GRANTED) {

                    final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Audio", "Cancel"};
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Select Option");
                    builder.setItems(options, new DialogInterface.OnClickListener() {
                        @RequiresApi(api = Build.VERSION_CODES.M)
                        @Override
                        public void onClick(DialogInterface dialog, int item) {
                            if (options[item].equals("Take Photo")) {
                                dialog.dismiss();
                                capture_image();
                            } else if (options[item].equals("Choose From Gallery")) {
                                dialog.dismiss();
                                Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(pickPhoto, GALLERY_PIC_REQUEST);
                            } /*else if (options[item].equals("Audio")) {
                                dialog.dismiss();
                                Intent intent_upload = new Intent();
                                intent_upload.setAction(Intent.ACTION_GET_CONTENT);
                                intent_upload.setType("audio/*");
                                startActivityForResult(intent_upload,155);
                            }*/ else if (options[item].equals("Cancel")) {
                                dialog.dismiss();
                            }
                        }
                    });
                    builder.show();
                } else {
                    requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);

                }
                //Toast.makeText(getActivity(), "Camera Permission error", Toast.LENGTH_SHORT).show();


            } catch (Exception e) {
                Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        } else {

            try {

                final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Cancel"};
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Select Option");
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (options[item].equals("Take Photo")) {
                            dialog.dismiss();
                            capture_image();
                        } else if (options[item].equals("Choose From Gallery")) {
                            dialog.dismiss();
                            Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(pickPhoto, GALLERY_PIC_REQUEST);
                        } else if (options[item].equals("Cancel")) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            } catch (Exception e) {
                // Toast.makeText(getActivity(), "Camera Permission error", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void capture_image() {

        if (Build.VERSION.SDK_INT > 21) { //use this if Lollipop_Mr1 (API 22) or above

            if (ActivityCompat.checkSelfPermission(MessageActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(MessageActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(MessageActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                Intent callCameraApplicationIntent = new Intent();
                callCameraApplicationIntent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);
                File photoFile = null;
                photoFile = createImageFile();
                Uri outputUri = FileProvider.getUriForFile(MessageActivity.this, BuildConfig.APPLICATION_ID + ".provider", photoFile);
                callCameraApplicationIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri);
                callCameraApplicationIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                Logger.getAnonymousLogger().info("Calling the camera App by intent");
                startActivityForResult(callCameraApplicationIntent, CAMERA_PIC_REQUEST);
            } else {
                requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);
            }
        } else {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
            startActivityForResult(intent, CAMERA_PIC_REQUEST);
        }

    }


    private File createImageFile() {
        Logger.getAnonymousLogger().info("Generating the image - method started");
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmSS").format(new Date());
        String imageFileName = "IMAGE_" + timeStamp;
        File storageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES + "/photo_saving_app");
        Logger.getAnonymousLogger().info("Storage directory set");
        if (!storageDirectory.exists()) storageDirectory.mkdir();
        File image = new File(storageDirectory, imageFileName + ".jpg");
        Logger.getAnonymousLogger().info("File name and path set");
        mImageFileLocation = image.getAbsolutePath();
        fileUri = Uri.parse(mImageFileLocation);
        return image;
    }


    /**
     * Creating file uri to store image/video
     */
    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /**
     * returning image / video
     */
    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("ss", "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    private void darawTimestamponImage(String path) {

        Bitmap bitmap = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 2;
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;

        try {
            File outfile = new File(path);
            bitmap = BitmapFactory.decodeStream(new FileInputStream(outfile), null, options);
            ExifInterface ei = null;
            try {
                ei = new ExifInterface(path);
            } catch (IOException e) {
                e.printStackTrace();
            }
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);

            Bitmap rotatedBitmap = null;
            switch (orientation) {

                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotatedBitmap = rotateImage(bitmap, 90);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotatedBitmap = rotateImage(bitmap, 180);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotatedBitmap = rotateImage(bitmap, 270);
                    break;

                case ExifInterface.ORIENTATION_NORMAL:
                default:
                    rotatedBitmap = bitmap;
            }

            Bitmap mutableBitmap = rotatedBitmap.copy(Bitmap.Config.ARGB_8888, true);
            // Utilities.drawTimeStampOnImage(mutableBitmap, outfile.getAbsolutePath());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_PIC_REQUEST && resultCode == RESULT_OK) {
            if (Build.VERSION.SDK_INT > 21) {
                file_path = mImageFileLocation;
                darawTimestamponImage(file_path);
                Log.e("msg", "path1 " + file_path);
                binding.fileTxt.setText(file_path);
                sharedPreferenceDataUtil.setImagePath(file_path);

            } else {
                file_path = fileUri.getPath();
                darawTimestamponImage(file_path);
                Log.e("msg", "path1 " + file_path);
                binding.fileTxt.setText(file_path);
                sharedPreferenceDataUtil.setImagePath(file_path);
            }


        } else if (requestCode == GALLERY_PIC_REQUEST && resultCode == RESULT_OK) {

            if (data != null) {

                Uri selectedImage = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = this.getContentResolver().query(selectedImage, filePath,
                        null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String selectedImagePath = c.getString(columnIndex);
                c.close();

                file_path = selectedImagePath;
                binding.fileTxt.setText(file_path);
                sharedPreferenceDataUtil.setImagePath(file_path);

                if (!file_path.equals("")) {
                } else {
                    Toast.makeText(this, "image not exist", Toast.LENGTH_SHORT).show();
                    // Glide.with(this).load(file_path).into(image_child);
                }

                Log.e("msg", "path1 " + selectedImagePath);

            } else {
                Toast.makeText(this, "Cancelled",
                        Toast.LENGTH_SHORT).show();
            }

        }else  if (requestCode == 155 && resultCode == RESULT_OK) {

            Uri i = data.getData();  // getData
            String s = i.getPath(); // getPath
            File k = new File(s);  // set File from path
            if (s != null) {
                ContentValues values = new ContentValues();
                values.put(MediaStore.MediaColumns.DATA, k.getAbsolutePath());
                values.put(MediaStore.MediaColumns.TITLE, "ring");
                values.put(MediaStore.MediaColumns.MIME_TYPE, "audio/*");
                values.put(MediaStore.MediaColumns.SIZE, k.length());
                values.put(MediaStore.Audio.Media.ARTIST, R.string.app_name);
                values.put(MediaStore.Audio.Media.IS_RINGTONE, true);
                values.put(MediaStore.Audio.Media.IS_NOTIFICATION, true);
                values.put(MediaStore.Audio.Media.IS_ALARM, true);
                values.put(MediaStore.Audio.Media.IS_MUSIC, false);

                Uri uri = MediaStore.Audio.Media.getContentUriForPath(k
                        .getAbsolutePath());
                getContentResolver().delete(
                        uri,
                        MediaStore.MediaColumns.DATA + "=\""
                                + k.getAbsolutePath() + "\"", null);
                Uri newUri = getContentResolver().insert(uri, values);

                file_path = newUri.getPath();

                binding.fileTxt.setText(newUri.toString());
            }

        }
    }

    private String _getRealPathFromURI(Context context, Uri contentUri) {
        String[] proj = { MediaStore.Audio.Media.DATA };
        CursorLoader loader = new CursorLoader(context, contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private void getMessageData() {

        showCustomLoading("");
        viewModel.sendMessageData(MessageActivity.this, sharedPreferenceDataUtil.getUserId(),
                sharedPreferenceDataUtil.getVillageId(), "3",
                binding.etMsg.getText().toString().trim(), file_path, catId,getPreferToken(SharedPreferenceDataUtil.refreshedToken));
    }

    private void getGalleryData(String villageId, String catId) {

        showCustomLoading("");
        galleryViewModel.getGallery(MessageActivity.this, villageId, this.catId);
    }

}