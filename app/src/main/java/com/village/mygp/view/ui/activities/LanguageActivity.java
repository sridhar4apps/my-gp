package com.village.mygp.view.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RadioButton;

import com.village.mygp.BuildConfig;
import com.village.mygp.R;
import com.village.mygp.databinding.ActivityLanguageBinding;
import com.village.mygp.databinding.LayoutComplaintBinding;
import com.village.mygp.view.baseclasses.BaseActivity;
import com.village.mygp.view.utilities.FlavorName;
import com.village.mygp.view.utilities.LocaleManager;
import com.village.mygp.view.utilities.SharedPreferenceDataUtil;
import com.village.mygp.view.utilities.UtilManager;

public class LanguageActivity extends BaseActivity implements RadioButton.OnCheckedChangeListener, View.OnClickListener {

    public static Intent build(Activity activity) {
        return new Intent(activity, LanguageActivity.class);
    }
    public static Intent build(Activity activity,String from) {
        Intent build = new Intent(activity,LanguageActivity.class);
        build.putExtra("From",from);
        return build;
    }
    private ActivityLanguageBinding binding;
    private SharedPreferenceDataUtil prefs;
    boolean checkSaveStatus = false;
    int selectedPosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(LanguageActivity.this, R.layout.activity_language);
        initializeRadioButtons();
        prefs = new SharedPreferenceDataUtil(LanguageActivity.this);
        binding.backIcon.setOnClickListener(v ->
                onBackPressed());
        if(getIntent().getExtras()!=null && getIntent().getExtras().getString("From").equalsIgnoreCase("languages")) {
            binding.backIcon.setVisibility(View.GONE);
        }else{
            binding.backIcon.setVisibility(View.VISIBLE);
        }

        binding.btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkSaveStatus = true;
                saveSelectedLanguage(selectedPosition);
                Intent build;
                if(getIntent().getExtras()!=null && getIntent().getExtras().getString("From").equalsIgnoreCase("languages")) {
                    prefs.setLanguageSelect(true);
                    if (FlavorName.SARPANCH.name().equalsIgnoreCase(BuildConfig.FLAVOR) || FlavorName.TRSSARPANCH.name().equalsIgnoreCase(BuildConfig.FLAVOR)) {
                        if(prefs.getLocationSelect())
                        {
                            build = MainActivity.build(LanguageActivity.this);
                        }else{

                            build = SarpanchLoginActivity.build(LanguageActivity.this);
                        }

                    } else {
                        if(prefs.getLocationSelect())
                        {
                            build = MainActivity.build(LanguageActivity.this);
                        }else{

                            build = LocalityActivity.build(LanguageActivity.this);
                        }

                    }
                    startActivity(build);
                    finish();
                }else{
                    UtilManager.clearStackLaunchHomeScreen(LanguageActivity.this);
                }

            }
        });

        checkSelectedLanguage();
    }

    private void initializeRadioButtons() {

        binding.englishBtn.setOnCheckedChangeListener(this);
        binding.teluguBtn.setOnCheckedChangeListener(this);

        binding.englishLayout.setOnClickListener(this);
        binding.teluguLayout.setOnClickListener(this);
    }

    private void checkSelectedLanguage() {
        //  selectedPosition = prefs.getCheckedLangauge();
        String selectedLanguage = prefs.getSelectedLanguage();
        if (selectedLanguage.equalsIgnoreCase("en")) {
            selectedPosition = 0;
        } else if (selectedLanguage.equalsIgnoreCase("te")) {
            selectedPosition = 2;
        }
        checkSaveStatus = true;
        showSelectedLanguage(selectedPosition);
    }

    private void showSelectedLanguage(int selectedPosition) {
        if (selectedPosition == 0) {
            binding.englishBtn.setChecked(true);
            clearLanguageCheck(R.id.englishBtn);
        } else if (selectedPosition == 2) {
            binding.teluguBtn.setChecked(true);
            clearLanguageCheck(R.id.teluguBtn);
        }
    }

    private void clearLanguageCheck(int id) {
        if (id == R.id.englishBtn) {
            binding.teluguGroup.clearCheck();
            selectedPosition = 0;
            saveSelectedLanguage(selectedPosition);
        } else if (id == R.id.teluguBtn) {
            binding.englishGroup.clearCheck();
            selectedPosition = 2;
            saveSelectedLanguage(selectedPosition);
        }
    }

    private void saveSelectedLanguage(int selectedPosition) {
        if (checkSaveStatus) {
            //prefs.setSelectedLangaugePosition(selectedPosition);
            // prefs.commit();
            if (selectedPosition == 0) {
                LocaleManager.setNewLocale(this, "en");
                prefs.setSelectedLanguage("en", "IN");
                prefs.commit();
            } else if (selectedPosition == 2) {
                LocaleManager.setNewLocale(this, "te");
                prefs.setSelectedLanguage("te", "IN");
                prefs.commit();
            }
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        checkSaveStatus = false;
        clearLanguageCheck(compoundButton.getId());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.englishLayout:
                binding.englishBtn.setChecked(true);
                break;

            case R.id.teluguLayout:
                binding.teluguBtn.setChecked(true);
                break;
            default:
                binding.englishBtn.setChecked(true);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}