package com.village.mygp.view.ui.dialog;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.databinding.DataBindingUtil;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.village.mygp.R;
import com.village.mygp.databinding.SingleBtnDialogBinding;

import java.io.Serializable;


public class SingleButtonAlertDialog extends BottomSheetDialog {
    private ButtonClick buttonClick;
    public  Bundle argument;
    boolean callSupport;
    View view;
    Context context;
    public SingleButtonAlertDialog(@NonNull Context context, int theme) {
        super(context, theme);
    }
    public SingleButtonAlertDialog(@NonNull Context context, Bundle arguments) {
        super(context, R.style.BottomSheetDialog);
        argument = arguments;
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = getLayoutInflater();
        SingleBtnDialogBinding binding =  DataBindingUtil.inflate(inflater,R.layout.single_btn_dialog,null,false);
        // builder.setView(dialogView);
        view = binding.getRoot();
        setContentView(view);
        setViewHeight();

        if(argument!=null){
            String msg = argument.getString("msg");
            String okText = argument.getString("yesText");
            buttonClick = (ButtonClick)argument.get("listener");
            if(msg!=null && !msg.isEmpty()) {
                binding.message.setText(msg);

            }
            Activity activity = (Activity) argument.get("activity");
            callSupport = argument.getBoolean("callSupport");

            if(buttonClick!=null)
            {
                setCancelable(false);
            }


            if(okText!=null&& !TextUtils.isEmpty(okText))
                binding.oktxt.setText(okText);
            if(msg!=null && !TextUtils.isEmpty(msg)){
                binding.message.setText(Html.fromHtml(msg));
            }
        }
        binding.ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(buttonClick!= null){
                    buttonClick.onClick(v);
                    dismiss();
                }else{
                    if(callSupport){
                        getOwnerActivity().finish();
                    }else
                        dismiss();
                }
            }
        });
    }
    public interface ButtonClick extends Serializable{
        void onClick(View v);
    }
    private void setViewHeight() {
        view.post(() -> {
            View parent = (View) view.getParent();
            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) (parent).getLayoutParams();
            CoordinatorLayout.Behavior behavior = params.getBehavior();
            BottomSheetBehavior bottomSheetBehavior = (BottomSheetBehavior) behavior;
            bottomSheetBehavior.setPeekHeight(view.getMeasuredHeight());
             //bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            // ((View)bottomSheet.getParent()).setBackgroundColor(Color.TRANSPARENT);

        });
        setCancelable(false);
    }
}
