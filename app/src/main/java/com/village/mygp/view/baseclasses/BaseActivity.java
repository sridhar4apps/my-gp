package com.village.mygp.view.baseclasses;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.village.mygp.R;
import com.village.mygp.view.dialog.CustomProgressDialog;
import com.village.mygp.view.utilities.AppThemes;
import com.village.mygp.view.utilities.LocaleManager;
import com.village.mygp.view.utilities.SharedPreferenceDataUtil;
import com.village.mygp.view.utilities.UtilManager;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


@SuppressLint("Registered")
public class BaseActivity extends AppCompatActivity {

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocaleManager.setLocale(newBase));
    }
    CustomProgressDialog mProgressDialog = null;
    private ProgressDialog progressDialog;
    NetworkInfo activeNetworkInfo;
    private SharedPreferenceDataUtil sharedPreferenceDataUtil;
    private AppThemes appThemes;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ConnectivityManager connectivityManager = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        sharedPreferenceDataUtil = new SharedPreferenceDataUtil(this);
        appThemes = sharedPreferenceDataUtil.getAppTheme();
        setAppTheme(appThemes);


    }

    private void setAppTheme(AppThemes appThemes) {
        switch (appThemes) {
            case TDP:
                setTheme(R.style.Theme_TDP);
                break;

            default:
                setTheme(R.style.Theme_MyGp);
                break;
        }
    }


    public boolean isNetworkAvailable() {
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    public void snakeBarView(View view, String msg) {
        Snackbar.make(view, msg, Snackbar.LENGTH_SHORT).show();
    }

    public void showDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        if (!progressDialog.isShowing())
            progressDialog.show();
    }

    public void dismissDialog() {
        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }

    public String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }


    public String getDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    public void showCustomLoading(String message) {
        if (mProgressDialog == null) {
            mProgressDialog = new CustomProgressDialog(this);
            mProgressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        }
        if (mProgressDialog != null && !mProgressDialog.isShowing()) {
            // mProgressDialog.setMessage(message);
            mProgressDialog.setTitle(null);
            mProgressDialog.setCancelable(false);
            // mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
        }
    }

    public void hideCustomLoading() {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }

    public static boolean validatePhNo(Context context, EditText textInputEditText) {
        String regexp = "^[6789]\\d{9}$";
        String phno = textInputEditText.getText() + "";
        if (phno.isEmpty()) {
            //textInputEditText.setError("Please Enter Mobile Number");
            //textInputEditText.requestFocus();
            UtilManager.showToastMessage(context, "Please enter mobile number");

            return false;
        } else if (phno.length() > 10 || !phno.matches(regexp)) {
            //0 tex
            // tInputEditText.setError(" Please Enter valid Mobile number");
            UtilManager.showToastMessage(context, "Please enter valid mobile number");
            return false;
        }

        return true;
    }
    public  void setPreferToken(String TAG, String value) {
        SharedPreferences preferences = this.getApplicationContext().getSharedPreferences(SharedPreferenceDataUtil.MY_PREFERENCES1, MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(TAG, value);
        editor.apply();
    }

    public  String getPreferToken(String TAG) {
        SharedPreferences preferences = this.getApplicationContext().getSharedPreferences(SharedPreferenceDataUtil.MY_PREFERENCES1, MODE_PRIVATE);
        String value = preferences.getString(TAG, "");
        return value;
    }
}
