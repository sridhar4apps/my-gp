package com.village.mygp.view.ui.activities;

import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DiffUtil;

import android.os.Build;
import android.os.Bundle;
import android.view.View;

import com.village.mygp.R;
import com.village.mygp.databinding.ActivityGalleryBinding;
import com.village.mygp.model.GalleryModel;
import com.village.mygp.view.baseclasses.BaseActivity;
import com.village.mygp.view.baseclasses.ToastService;
import com.village.mygp.view.ui.adapters.GalleryAdapter;
import com.village.mygp.view.utilities.SharedPreferenceDataUtil;
import com.village.mygp.viewmodel.GalleryViewModel;

public class GalleryActivity extends BaseActivity implements GalleryAdapter.ItemClickListener {
    private ActivityGalleryBinding binding;
    private SharedPreferenceDataUtil sharedPreferenceDataUtil;
    private ToastService toastService;
    private GalleryViewModel galleryViewModel;
    private GalleryAdapter adapter;
    private String catId;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(GalleryActivity.this, R.layout.activity_gallery);
        this.toastService = new ToastService(this);
        sharedPreferenceDataUtil = new SharedPreferenceDataUtil(this);
        galleryViewModel = new ViewModelProvider(this).get(GalleryViewModel.class);
        binding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        Bundle bundle = getIntent().getExtras();
        binding.complaintName.setText(bundle.getString("types"));
        catId = String.valueOf(bundle.getInt("value"));

        adapter = new GalleryAdapter((DiffUtil.ItemCallback<GalleryModel.Result>) null, this);
        if (isNetworkAvailable()) {
            getGalleryData(sharedPreferenceDataUtil.getVillageId());
        } else {
            toastService.info("No Internet!");
        }


        galleryViewModel.getData().observe(GalleryActivity.this, new Observer<GalleryModel>() {
            @Override
            public void onChanged(GalleryModel qaModel) {
                if (qaModel.getStatusCode() != null && qaModel.getStatusCode() == 200) {
                    if (qaModel.getResult() != null && qaModel.getResult().size() > 0) {
                        binding.recyclerView.setVisibility(View.VISIBLE);
                        binding.empty.setVisibility(View.GONE);
                        adapter.submitList(qaModel.getResult());
                        binding.recyclerView.setAdapter(adapter);
                    }
                } else {
                    binding.recyclerView.setVisibility(View.GONE);
                    binding.empty.setVisibility(View.VISIBLE);
                }
                hideCustomLoading();
            }
        });

        galleryViewModel.getMessageToShow().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                toastService.danger(s);
                hideCustomLoading();

                binding.recyclerView.setVisibility(View.GONE);
                binding.empty.setVisibility(View.VISIBLE);
            }
        });

    }

    private void getGalleryData(String villageId) {

        showCustomLoading("");
        galleryViewModel.getGallery(GalleryActivity.this, villageId, catId);
    }

    @Override
    public void itemClick(int i) {

    }
}