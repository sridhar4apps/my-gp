package com.village.mygp.model;

import java.util.Date;

public class Welfare {
    private Date createdAt;
    private String id;
    private String image;
    private String title;
    private int icon;

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Welfare(String image, String title, int icon) {
        this.image = image;
        this.title = title;
        this.icon = icon;
    }

    public Welfare() {

    }

    public Welfare(String str, Date date) {
        this.image = str;
        this.createdAt = date;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String str) {
        this.id = str;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String str) {
        this.image = str;
    }

    public Date getCreatedAt() {
        return this.createdAt;
    }

    public void setCreatedAt(Date date) {
        this.createdAt = date;
    }
}
