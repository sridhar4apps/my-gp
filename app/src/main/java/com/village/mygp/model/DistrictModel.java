package com.village.mygp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DistrictModel {

    @SerializedName("status_code")
    @Expose
    private Integer statusCode;
    @SerializedName("status_message")
    @Expose
    private String statusMessage;
    @SerializedName("result")
    @Expose
    private List<Result> result = null;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    public static class Result {

        @SerializedName("districtid")
        @Expose
        private String districtid;
        @SerializedName("district_title")
        @Expose
        private String districtTitle;
        @SerializedName("state_id")
        @Expose
        private String stateId;
        @SerializedName("district_description")
        @Expose
        private String districtDescription;
        @SerializedName("district_status")
        @Expose
        private String districtStatus;

        public String getDistrictid() {
            return districtid;
        }

        public void setDistrictid(String districtid) {
            this.districtid = districtid;
        }

        public String getDistrictTitle() {
            return districtTitle;
        }

        public void setDistrictTitle(String districtTitle) {
            this.districtTitle = districtTitle;
        }

        public String getStateId() {
            return stateId;
        }

        public void setStateId(String stateId) {
            this.stateId = stateId;
        }

        public String getDistrictDescription() {
            return districtDescription;
        }

        public void setDistrictDescription(String districtDescription) {
            this.districtDescription = districtDescription;
        }

        public String getDistrictStatus() {
            return districtStatus;
        }

        public void setDistrictStatus(String districtStatus) {
            this.districtStatus = districtStatus;
        }

    }

}
