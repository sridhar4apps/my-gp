package com.village.mygp.model;

public class Card {
    private int icon;
    private int category;
    private String title;

    public Card(String title, int category, int icon) {
        this.icon = icon;
        this.category = category;
        this.title = title;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public String getTitle() {
        return this.title;
    }

    public int getIcon() {
        return this.icon;
    }
}
