package com.village.mygp.viewmodel;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.village.mygp.model.ComplaintListModel;
import com.village.mygp.model.SarpanchModel;
import com.village.mygp.repository.ComplaintListRepository;
import com.village.mygp.view.ui.activities.ComplaintListActivity;

public class ComplaintListViewModel extends ViewModel {

    private final MutableLiveData<ComplaintListModel> mutableLiveData;
    private final MutableLiveData<String> messageToShow;
    private final ComplaintListRepository repository;

    public ComplaintListViewModel() {
        repository = new ComplaintListRepository();
        this.mutableLiveData = repository.getMutableLiveData();
        this.messageToShow = repository.getMessageToShow();
    }

    public MutableLiveData<ComplaintListModel> getData() {
        return mutableLiveData;
    }

    public MutableLiveData<String> getMessageToShow() {
        return messageToShow;
    }

    public void getVillageComplains(Context activity, String villageId) {
        repository.getComplaints(activity,villageId);
    }
}
