package com.village.mygp.viewmodel;

import android.content.Context;
import android.view.View;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.village.mygp.model.ComplaintListModel;
import com.village.mygp.model.ComplaintModel;
import com.village.mygp.repository.ComplaintListRepository;
import com.village.mygp.repository.ComplaintRepository;
import com.village.mygp.view.ui.activities.ComplaintsActivity;

public class ComplaintViewModel extends ViewModel {

    private final MutableLiveData<ComplaintModel> mutableLiveData;
    private final MutableLiveData<String> messageToShow;
    private final ComplaintRepository repository;

    public ComplaintViewModel() {
        repository = new ComplaintRepository();
        this.mutableLiveData = repository.getMutableLiveData();
        this.messageToShow = repository.getMessageToShow();
    }

    public MutableLiveData<ComplaintModel> getData() {
        return mutableLiveData;
    }

    public MutableLiveData<String> getMessageToShow() {
        return messageToShow;
    }
    public void submitData(Context activity, String userId, String villageId, String catValue, String name, String mobile, String ward, String address, String complaint, String notifyToken) {
        repository.submitComplaintData(activity,userId,villageId,catValue,name,mobile,ward,address,complaint,notifyToken);
    }
}
