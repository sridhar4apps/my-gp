package com.village.mygp.viewmodel;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.village.mygp.model.SarpanchModel;
import com.village.mygp.repository.LoginRepository;

public class LoginViewModel extends ViewModel {

    private final MutableLiveData<SarpanchModel> sarpanchModelMutableLiveData;
    private final MutableLiveData<String> messageToShow;
    private final LoginRepository loginRepository;

    public LoginViewModel() {
        loginRepository = new LoginRepository();

        sarpanchModelMutableLiveData = loginRepository.getMutableLiveData();
        messageToShow = loginRepository.getMessageToShow();
    }

    public MutableLiveData<SarpanchModel> getLoginData() {
        return sarpanchModelMutableLiveData;
    }

    public MutableLiveData<String> getMessageToShow() {
        return messageToShow;
    }

    public void getSarpanch(Context activity, String mobile, String email) {
        loginRepository.getSarpanchData(activity,mobile,email);

    }
}
