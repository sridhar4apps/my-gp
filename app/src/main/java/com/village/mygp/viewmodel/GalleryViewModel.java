package com.village.mygp.viewmodel;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.village.mygp.model.GalleryModel;
import com.village.mygp.repository.GalleryRepository;

public class GalleryViewModel extends ViewModel {

    private final MutableLiveData<GalleryModel> mutableLiveData;
    private final MutableLiveData<String> messageToShow;
    private final GalleryRepository repository;

    public GalleryViewModel() {
        repository = new GalleryRepository();
        mutableLiveData = repository.getMutableLiveData();
        messageToShow = repository.getMessageToShow();
    }

    public MutableLiveData<GalleryModel> getData() {
        return mutableLiveData;
    }

    public MutableLiveData<String> getMessageToShow() {
        return messageToShow;
    }

    public void getGallery(Context activity, String villageId, String catId) {
        repository.getGallerData(activity,villageId,catId);
    }
}
