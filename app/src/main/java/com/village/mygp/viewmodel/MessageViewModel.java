package com.village.mygp.viewmodel;

import android.content.Context;
import android.view.View;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.village.mygp.model.MessageModel;
import com.village.mygp.model.SarpanchModel;
import com.village.mygp.repository.MessageRespository;
import com.village.mygp.view.ui.activities.MessageActivity;

public class MessageViewModel extends ViewModel {


    private final MutableLiveData<MessageModel> mutableLiveData;
    private final MutableLiveData<String> messageToShow;
    private final MessageRespository respository;

    public MessageViewModel() {
        respository = new MessageRespository();
        mutableLiveData = respository.getMutableLiveData();
        messageToShow = respository.getMessageToShow();
    }

    public MutableLiveData<MessageModel> getMessageData() {
        return mutableLiveData;
    }

    public MutableLiveData<String> getMessageToShow() {
        return messageToShow;
    }

    public void sendMessageData(Context activity, String userId, String villageId, String type, String msg, String file_path, String catId, String preferToken) {
        respository.submitMessage(activity,userId,villageId,type,msg,file_path,catId,preferToken);
    }
}
