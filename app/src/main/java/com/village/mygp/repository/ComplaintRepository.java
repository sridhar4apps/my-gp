package com.village.mygp.repository;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;

import com.village.mygp.application.App;
import com.village.mygp.model.ComplaintListModel;
import com.village.mygp.model.ComplaintModel;
import com.village.mygp.view.api.RetrofitApis;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ComplaintRepository {
    private final MutableLiveData<ComplaintModel> mutableLiveData;
    private final MutableLiveData<String> messageToShow;
    private final RetrofitApis retrofitApis;
    public ComplaintRepository() {
        mutableLiveData = new MutableLiveData<>();
        messageToShow = new MutableLiveData<>();
        retrofitApis = RetrofitApis.Factory.create();
    }

    public MutableLiveData<ComplaintModel> getMutableLiveData() {
        return mutableLiveData;
    }

    public MutableLiveData<String> getMessageToShow() {
        if (messageToShow == null)
            messageToShow.setValue("");
        return messageToShow;
    }

    public void submitComplaintData(Context activity, String userId, String villageId, String catValue, String name, String mobile, String ward, String address, String complaint, String notifyToken) {
        Call<ComplaintModel> modelCall = retrofitApis.complaintSubmit("0",villageId,catValue,name,mobile,ward,address,complaint,notifyToken);
        modelCall.enqueue(new Callback<ComplaintModel>() {
            @Override
            public void onResponse(Call<ComplaintModel> call, Response<ComplaintModel> response) {
                if (response.isSuccessful()){
                    ComplaintModel qaModel = response.body();
                    if (qaModel != null) {
                        mutableLiveData.setValue(qaModel);
                    } else {
                        messageToShow.setValue(qaModel.getStatusMessage());
                    }
                }else {
                    messageToShow.setValue(response.message());
                }
            }

            @Override
            public void onFailure(Call<ComplaintModel> call, Throwable t) {
                if (App.getInstance().isNetworkAvailable(activity)) {
                    messageToShow.setValue("Something went wrong! please contact your administrator");
                } else {
                    messageToShow.setValue("Please check your internet connection!");
                }
            }
        });
    }
}
