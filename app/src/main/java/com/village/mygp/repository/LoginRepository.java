package com.village.mygp.repository;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;

import com.village.mygp.application.App;
import com.village.mygp.model.SarpanchModel;
import com.village.mygp.view.api.RetrofitApis;
import com.village.mygp.viewmodel.LoginViewModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginRepository {

    private final MutableLiveData<SarpanchModel> mutableLiveData;
    private final MutableLiveData<String> messageToShow;
    private final RetrofitApis retrofitApis;

    public LoginRepository() {
        mutableLiveData = new MutableLiveData<>();
        messageToShow = new MutableLiveData<>();
        retrofitApis = RetrofitApis.Factory.create();

    }

    public MutableLiveData<SarpanchModel> getMutableLiveData() {
        return mutableLiveData;
    }

    public MutableLiveData<String> getMessageToShow() {
        if (messageToShow == null)
            messageToShow.setValue("");
        return messageToShow;
    }

    public void getSarpanchData(Context activity, String mobile, String email) {

        Call<SarpanchModel> modelCall = retrofitApis.getSarpanchLogin(mobile, email);
        modelCall.enqueue(new Callback<SarpanchModel>() {
            @Override
            public void onResponse(Call<SarpanchModel> call, Response<SarpanchModel> response) {
                if (response.isSuccessful()) {
                    SarpanchModel qaModel = response.body();
                    if (qaModel != null && qaModel.getSuccess().equalsIgnoreCase("true")) {
                        mutableLiveData.setValue(qaModel);
                    } else {
                        messageToShow.setValue(qaModel.getMessage());
                    }
                } else {
                    messageToShow.setValue(response.message());
                }
            }

            @Override
            public void onFailure(Call<SarpanchModel> call, Throwable t) {
                if (App.getInstance().isNetworkAvailable(activity)) {
                    messageToShow.setValue("Something went wrong! please contact your administrator");
                } else {
                    messageToShow.setValue("Please check your internet connection!");
                }
            }
        });
    }
}
