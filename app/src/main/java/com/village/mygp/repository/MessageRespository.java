package com.village.mygp.repository;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.village.mygp.application.App;
import com.village.mygp.model.MessageModel;
import com.village.mygp.model.SarpanchModel;
import com.village.mygp.view.api.RetrofitApis;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MessageRespository {
    private final MutableLiveData<MessageModel> mutableLiveData;
    private final MutableLiveData<String> messageToShow;
    private final RetrofitApis retrofitApis;

    public MessageRespository() {
        mutableLiveData = new MutableLiveData<>();
        messageToShow = new MutableLiveData<>();
        retrofitApis = RetrofitApis.Factory.create();
    }

    public MutableLiveData<MessageModel> getMutableLiveData() {
        return mutableLiveData;
    }

    public MutableLiveData<String> getMessageToShow() {
        if (messageToShow == null)
            messageToShow.setValue("");
        return messageToShow;
    }

    public void submitMessage(Context activity, String userId, String villageId, String type, String msg, String file_path, String catId, String preferToken) {
        RequestBody userID = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(userId));
        RequestBody villageID = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(villageId));
        RequestBody typeID = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(type));
        RequestBody msgSend = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(msg));
        RequestBody categoryId = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(catId));
        RequestBody token = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(preferToken));

        MultipartBody.Part ImagePhoto = null;
        String path2 = file_path;
        if (path2 != null) {
            File file = new File(path2);
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            ImagePhoto = MultipartBody.Part.createFormData("uploaded_file", file.getName(), requestFile);
        } else {
            RequestBody attachmentEmpty = RequestBody.create(MediaType.parse("text/plain"), "");
            ImagePhoto = MultipartBody.Part.createFormData("uploaded_file", "", attachmentEmpty);
        }

        Call<MessageModel> modelCall = retrofitApis.messageSubmit(userID, villageID, msgSend, typeID, categoryId, token, ImagePhoto);
        modelCall.enqueue(new Callback<MessageModel>() {
            @Override
            public void onResponse(@NonNull Call<MessageModel> call, @NonNull Response<MessageModel> response) {
                if (response.isSuccessful()) {
                    MessageModel qaModel = response.body();
                    if (qaModel != null) {
                        mutableLiveData.setValue(qaModel);
                    } else {
                        messageToShow.setValue(qaModel.getStatusMessage());
                    }
                } else {
                    messageToShow.setValue(response.message());
                }
            }

            @Override
            public void onFailure(@NonNull Call<MessageModel> call, @NonNull Throwable t) {
                if (App.getInstance().isNetworkAvailable(activity)) {
                    messageToShow.setValue("Something went wrong! please contact your administrator");
                } else {
                    messageToShow.setValue("Please check your internet connection!");
                }
            }
        });
    }
}
