package com.village.mygp.repository;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;

import com.village.mygp.application.App;
import com.village.mygp.model.ComplaintListModel;
import com.village.mygp.model.GalleryModel;
import com.village.mygp.view.api.RetrofitApis;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GalleryRepository {
    private final MutableLiveData<GalleryModel> mutableLiveData;
    private final MutableLiveData<String> messageToShow;
    private final RetrofitApis retrofitApis;

    public GalleryRepository() {
        mutableLiveData = new MutableLiveData<>();
        messageToShow = new MutableLiveData<>();
        retrofitApis = RetrofitApis.Factory.create();
    }


    public MutableLiveData<GalleryModel> getMutableLiveData() {
        return mutableLiveData;
    }

    public MutableLiveData<String> getMessageToShow() {
        if (messageToShow == null)
            messageToShow.setValue("");
        return messageToShow;
    }

    public void getGallerData(Context activity, String villageId, String catId) {
        Call<GalleryModel> modelCall = retrofitApis.getGalleryData(villageId,catId);
        modelCall.enqueue(new Callback<GalleryModel>() {
            @Override
            public void onResponse(Call<GalleryModel> call, Response<GalleryModel> response) {
                if (response.isSuccessful()) {
                    GalleryModel qaModel = response.body();
                    if (qaModel != null) {
                        mutableLiveData.setValue(qaModel);
                    } else {
                        messageToShow.setValue(qaModel.getStatusMessage());
                    }
                } else {
                    messageToShow.setValue(response.message());
                }
            }

            @Override
            public void onFailure(Call<GalleryModel> call, Throwable t) {
                if (App.getInstance().isNetworkAvailable(activity)) {
                    messageToShow.setValue("Something went wrong! please contact your administrator");
                } else {
                    messageToShow.setValue("Please check your internet connection!");
                }
            }
        });
    }
}
