package com.village.mygp.repository;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;

import com.village.mygp.application.App;
import com.village.mygp.model.ComplaintListModel;
import com.village.mygp.model.SarpanchModel;
import com.village.mygp.view.api.RetrofitApis;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ComplaintListRepository {

    private final MutableLiveData<ComplaintListModel> mutableLiveData;
    private final MutableLiveData<String> messageToShow;
    private final RetrofitApis retrofitApis;

    public ComplaintListRepository() {
        mutableLiveData = new MutableLiveData<>();
        messageToShow = new MutableLiveData<>();
        retrofitApis = RetrofitApis.Factory.create();

    }

    public MutableLiveData<ComplaintListModel> getMutableLiveData() {
        return mutableLiveData;
    }

    public MutableLiveData<String> getMessageToShow() {
        if (messageToShow == null)
            messageToShow.setValue("");
        return messageToShow;
    }

    public void getComplaints(Context activity, String villageId) {
        Call<ComplaintListModel> modelCall = retrofitApis.getVillageComplaints(villageId);
        modelCall.enqueue(new Callback<ComplaintListModel>() {
            @Override
            public void onResponse(Call<ComplaintListModel> call, Response<ComplaintListModel> response) {
                if (response.isSuccessful()){
                    ComplaintListModel qaModel = response.body();
                    if (qaModel != null) {
                        mutableLiveData.setValue(qaModel);
                    } else {
                        messageToShow.setValue(qaModel.getStatusMessage());
                    }
                }else {
                    messageToShow.setValue(response.message());
                }
            }

            @Override
            public void onFailure(Call<ComplaintListModel> call, Throwable t) {
                if (App.getInstance().isNetworkAvailable(activity)) {
                    messageToShow.setValue("Something went wrong! please contact your administrator");
                } else {
                    messageToShow.setValue("Please check your internet connection!");
                }
            }
        });
    }
}
